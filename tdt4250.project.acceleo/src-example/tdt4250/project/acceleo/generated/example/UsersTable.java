package tdt4250.project.acceleo.generated.example;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class UsersTable implements IMigration {
	private final String MIGRATION = "SELECT\n"
							+ "			Users.Uuid,\n"
							+ "			\n"
							+ "			Users.Birthdate,\n"
							+ "			Occupations.OccupationName\n"
							+ "		FROM Users \n"
							+ "		INNER JOIN Occupations \n"
							+ "		ON Users.UUID = Occupations.UserUUID\n"
							+ "	;";
	public UsersTable() {
		
	}
	
	public void executeMigration(Connection sourceConnection, Connection destinationConnection) throws SQLException { 
		Statement stmt = sourceConnection.createStatement();
		ResultSet rs = stmt.executeQuery(MIGRATION);
		
		LinkedList<UsersColumn> columns = new LinkedList<UsersColumn>();
		
		while(rs.next()) {
			System.out.println("Row");
			String uuid = rs.getString("Uuid");
			String birthdate = rs.getString("Birthdate");
			String occupationName = rs.getString("OccupationName");
			String name = ""; //TODO macro
			
			UsersColumn column = new UsersColumn(uuid, name, birthdate, occupationName);
			System.out.println(column);
			columns.add(column);
 		}
		
		//Now, insert them into the new database
		destinationConnection.setAutoCommit(false);
		for(UsersColumn column : columns) {
			PreparedStatement pstmt = destinationConnection.prepareStatement("INSERT INTO \"users\" ( \"uuid\",\n"
					+ "				\"name\",\n"
					+ "				\"birthdate\",\n"
					+ "				\"occupation_name\"\n"
					+ "		) VALUES ('" + column.getUuid() + "', ?, ?, ?)");
			//pstmt.set(1, column.getUuid());
			pstmt.setString(1, column.getName());
			pstmt.setDate(2, column.getBirthdate());
			pstmt.setString(3, column.getOccupation_name());
			
			boolean success = pstmt.execute();
			System.out.println("Success? " + success);
		}
		//Execute and commit
		destinationConnection.commit();
		destinationConnection.setAutoCommit(true);
	}
}
