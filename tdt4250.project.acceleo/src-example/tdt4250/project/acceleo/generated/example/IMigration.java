package tdt4250.project.acceleo.generated.example;

import java.sql.Connection;
import java.sql.SQLException;

public interface IMigration {
	public void executeMigration(Connection sourceConnection, Connection desinationConnection)  throws SQLException;
}
