package tdt4250.project.acceleo.generated.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GeneratorMain {

	public static void main(String[] args) {
		System.out.println("Hello");
		
		IMigration[] migrations = new IMigration[] {
				new UsersTable()
		};
		//Check for MySQL driver
		try {
		    Class.forName("com.mysql.jdbc.Driver");
		} 
		catch (ClassNotFoundException e) {
		    System.out.println("You do not have mysql driver installed");
		    e.printStackTrace();
		} 
		//Check for PSQL driver
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("You do not have a psql connection driver installed");
			e.printStackTrace();
		}

		
		// Set up db connections
		try {
			Connection sqlConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/tdt4250", "root", "sjallabajs");
			Connection psqlConn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/tdt4250", "tdt4250", "example");
			
			
			
			for(IMigration migration : migrations) {
				migration.executeMigration(sqlConn, psqlConn);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
