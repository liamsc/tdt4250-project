package tdt4250.project.acceleo.generated.example;

import java.sql.Date;

public class UsersColumn {
	private String uuid;
	private String name;
	private String birthdate;
	private String occupation_name;
	
	public UsersColumn(String uuid, String name, String birthdate, String occupation_name) {
		this.uuid = uuid;
		this.name = name;
		this.birthdate = birthdate;
		this.occupation_name = occupation_name;
	}
	
	@Override
	public String toString() {
		return "UsersColumn: " + this.uuid + " Born " + this.birthdate;
	}
	
	public String getUuid() {
		return this.uuid;
	}

	public String getOccupation_name() {
		return occupation_name;
	}

	public Date getBirthdate() {
		Date date = new java.sql.Date(0000-00-00);
		return date.valueOf(this.birthdate); //TODO maybe make this conversion earlier
	}

	public String getName() {
		return name;
	}
}
