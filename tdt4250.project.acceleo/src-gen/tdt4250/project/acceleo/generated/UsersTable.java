		package tdt4250.project.acceleo.generated;

		import java.sql.Connection;
		import java.sql.PreparedStatement;
		import java.sql.ResultSet;
		import java.sql.SQLException;
		import java.sql.Statement;
		import java.util.LinkedList;
		
		public class UsersTable implements IMigration {
			private final String MIGRATION = 	"SELECT"				+"Users.Uuid,"
						+"Users.Birthdate,"
						+"Occupations.OccupationName"
					+ "FROM Users "
					+ "INNER JOIN Occupations" 
					+ "ON Users.UUID == Occupations.UserUUID"
			+";";
			public UsersTable() {
				
			}
			public migration.impl.MacroColumnImpl@32fe9d0a (columnName: Name) (macroName: null)  ()
			
			
			public void executeMigration(Connection sourceConnection, Connection destinationConnection) throws SQLException { 
				Statement stmt = sourceConnection.createStatement();
				ResultSet rs = stmt.executeQuery(MIGRATION);
				
				LinkedList<UsersColumn> columns = new LinkedList<UsersColumn>();
				
				while(rs.next()) {
					System.out.println("Row");
					String Uuid = rs.getString("Uuid");
					String Name = "TODO this is a macro";
					String Birthdate = rs.getString("Birthdate");
					String OccupationName = rs.getString("OccupationName");
					
					UsersColumn column = new UsersColumn(
						Uuid,
						Name,
						Birthdate,
						OccupationName
					);
					System.out.println(column);
					columns.add(column);
		 		}
				
				//Now, insert them into the new database
				destinationConnection.setAutoCommit(false);
				for(UsersColumn column : columns) {
					PreparedStatement pstmt = destinationConnection.prepareStatement(	"INSERT INTO Users (	"		+ "`Uuid`,"
								+ "`Name`,"
								+ "`Birthdate`,"
								+ "`OccupationName`"
						+ " ) VALUES <values>;");

						pstmt.setString(2, column.getUuid());
						pstmt.setString(3, column.getName());
						pstmt.setString(4, column.getBirthdate());
						pstmt.setString(5, column.getOccupationName());
					
					boolean success = pstmt.execute();
					System.out.println("Success? " + success);
				}
				//Execute and commit
				destinationConnection.commit();
				destinationConnection.setAutoCommit(true);
			}
		}

