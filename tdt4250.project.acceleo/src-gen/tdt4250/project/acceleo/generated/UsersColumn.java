		package tdt4250.project.acceleo.generated;
		
		import java.sql.Date;
		
		public class UsersColumn {
			private String Uuid;
			private String Name;
			private String Birthdate;
			private String OccupationName;
			
			public UsersColumn(	String Uuid, 	String Name, 	String Birthdate, 	String OccupationName ) {
			this.Uuid = Uuid;
			this.Name = Name;
			this.Birthdate = Birthdate;
			this.OccupationName = OccupationName;
			}

			public String  getUuid() {
				return this.Uuid;
			}
			public String  getName() {
				return this.Name;
			}
			public String  getBirthdate() {
				return this.Birthdate;
			}
			public String  getOccupationName() {
				return this.OccupationName;
			}
			
		}

