		package tdt4250.project.acceleo.generated;

		import java.sql.Connection;
		import java.sql.PreparedStatement;
		import java.sql.ResultSet;
		import java.sql.SQLException;
		import java.sql.Statement;
		import java.util.LinkedList;
		
		public class ProductsTable implements IMigration {
			private final String MIGRATION = 	"SELECT"				+"Products.name,"
					+ "FROM Products "
			+";";
			public ProductsTable() {
				
			}
			public migration.impl.MacroColumnImpl@32057e6 (columnName: uuid) (macroName: null)  ()
			
			
			public void executeMigration(Connection sourceConnection, Connection destinationConnection) throws SQLException { 
				Statement stmt = sourceConnection.createStatement();
				ResultSet rs = stmt.executeQuery(MIGRATION);
				
				LinkedList<ProductsColumn> columns = new LinkedList<ProductsColumn>();
				
				while(rs.next()) {
					System.out.println("Row");
					String name = rs.getString("name");
					String uuid = "TODO this is a macro";
					
					ProductsColumn column = new ProductsColumn(
						name,
						uuid
					);
					System.out.println(column);
					columns.add(column);
		 		}
				
				//Now, insert them into the new database
				destinationConnection.setAutoCommit(false);
				for(ProductsColumn column : columns) {
					PreparedStatement pstmt = destinationConnection.prepareStatement(	"INSERT INTO Products (	"		+ "`name`,"
								+ "`uuid`"
						+ " ) VALUES <values>;");

						pstmt.setString(2, column.getname());
						pstmt.setString(3, column.getuuid());
					
					boolean success = pstmt.execute();
					System.out.println("Success? " + success);
				}
				//Execute and commit
				destinationConnection.commit();
				destinationConnection.setAutoCommit(true);
			}
		}

