		package tdt4250.project.acceleo.generated;
		
		import java.sql.Date;
		
		public class ProductsColumn {
			private String name;
			private String uuid;
			
			public ProductsColumn(	String name, 	String uuid ) {
			this.name = name;
			this.uuid = uuid;
			}

			public String  getname() {
				return this.name;
			}
			public String  getuuid() {
				return this.uuid;
			}
			
		}

