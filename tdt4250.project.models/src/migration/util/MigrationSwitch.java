/**
 */
package migration.util;

import migration.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see migration.MigrationPackage
 * @generated
 */
public class MigrationSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MigrationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MigrationSwitch() {
		if (modelPackage == null) {
			modelPackage = MigrationPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MigrationPackage.MIGRATION: {
				Migration migration = (Migration)theEObject;
				T result = caseMigration(migration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.FROM_TABLE: {
				FromTable fromTable = (FromTable)theEObject;
				T result = caseFromTable(fromTable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.TO_TABLE: {
				ToTable toTable = (ToTable)theEObject;
				T result = caseToTable(toTable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.MAPPING: {
				Mapping mapping = (Mapping)theEObject;
				T result = caseMapping(mapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.SINGLE_MAPPING: {
				SingleMapping singleMapping = (SingleMapping)theEObject;
				T result = caseSingleMapping(singleMapping);
				if (result == null) result = caseMapping(singleMapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.JOIN_MAPPING: {
				JoinMapping joinMapping = (JoinMapping)theEObject;
				T result = caseJoinMapping(joinMapping);
				if (result == null) result = caseMapping(joinMapping);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.FROM_COLUMN: {
				FromColumn fromColumn = (FromColumn)theEObject;
				T result = caseFromColumn(fromColumn);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.TO_COLUMN: {
				ToColumn toColumn = (ToColumn)theEObject;
				T result = caseToColumn(toColumn);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.REFERENCE_COLUMN: {
				ReferenceColumn referenceColumn = (ReferenceColumn)theEObject;
				T result = caseReferenceColumn(referenceColumn);
				if (result == null) result = caseToColumn(referenceColumn);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.MACRO_COLUMN: {
				MacroColumn macroColumn = (MacroColumn)theEObject;
				T result = caseMacroColumn(macroColumn);
				if (result == null) result = caseToColumn(macroColumn);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.CONNECTION_URI: {
				ConnectionURI connectionURI = (ConnectionURI)theEObject;
				T result = caseConnectionURI(connectionURI);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.MIGRATION_SOURCE: {
				MigrationSource migrationSource = (MigrationSource)theEObject;
				T result = caseMigrationSource(migrationSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MigrationPackage.MIGRATION_DESTINATION: {
				MigrationDestination migrationDestination = (MigrationDestination)theEObject;
				T result = caseMigrationDestination(migrationDestination);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Migration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Migration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMigration(Migration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>From Table</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>From Table</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFromTable(FromTable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>To Table</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>To Table</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseToTable(ToTable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMapping(Mapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Single Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Single Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingleMapping(SingleMapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Join Mapping</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Join Mapping</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJoinMapping(JoinMapping object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>From Column</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>From Column</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFromColumn(FromColumn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>To Column</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>To Column</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseToColumn(ToColumn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Column</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Column</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceColumn(ReferenceColumn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Macro Column</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Macro Column</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMacroColumn(MacroColumn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Connection URI</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Connection URI</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConnectionURI(ConnectionURI object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMigrationSource(MigrationSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Destination</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Destination</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMigrationDestination(MigrationDestination object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MigrationSwitch
