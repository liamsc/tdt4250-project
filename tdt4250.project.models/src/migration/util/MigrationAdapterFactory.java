/**
 */
package migration.util;

import migration.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see migration.MigrationPackage
 * @generated
 */
public class MigrationAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MigrationPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MigrationAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MigrationPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MigrationSwitch<Adapter> modelSwitch =
		new MigrationSwitch<Adapter>() {
			@Override
			public Adapter caseMigration(Migration object) {
				return createMigrationAdapter();
			}
			@Override
			public Adapter caseFromTable(FromTable object) {
				return createFromTableAdapter();
			}
			@Override
			public Adapter caseToTable(ToTable object) {
				return createToTableAdapter();
			}
			@Override
			public Adapter caseMapping(Mapping object) {
				return createMappingAdapter();
			}
			@Override
			public Adapter caseSingleMapping(SingleMapping object) {
				return createSingleMappingAdapter();
			}
			@Override
			public Adapter caseJoinMapping(JoinMapping object) {
				return createJoinMappingAdapter();
			}
			@Override
			public Adapter caseFromColumn(FromColumn object) {
				return createFromColumnAdapter();
			}
			@Override
			public Adapter caseToColumn(ToColumn object) {
				return createToColumnAdapter();
			}
			@Override
			public Adapter caseReferenceColumn(ReferenceColumn object) {
				return createReferenceColumnAdapter();
			}
			@Override
			public Adapter caseMacroColumn(MacroColumn object) {
				return createMacroColumnAdapter();
			}
			@Override
			public Adapter caseConnectionURI(ConnectionURI object) {
				return createConnectionURIAdapter();
			}
			@Override
			public Adapter caseMigrationSource(MigrationSource object) {
				return createMigrationSourceAdapter();
			}
			@Override
			public Adapter caseMigrationDestination(MigrationDestination object) {
				return createMigrationDestinationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link migration.Migration <em>Migration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.Migration
	 * @generated
	 */
	public Adapter createMigrationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.FromTable <em>From Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.FromTable
	 * @generated
	 */
	public Adapter createFromTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.ToTable <em>To Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.ToTable
	 * @generated
	 */
	public Adapter createToTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.Mapping <em>Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.Mapping
	 * @generated
	 */
	public Adapter createMappingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.SingleMapping <em>Single Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.SingleMapping
	 * @generated
	 */
	public Adapter createSingleMappingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.JoinMapping <em>Join Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.JoinMapping
	 * @generated
	 */
	public Adapter createJoinMappingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.FromColumn <em>From Column</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.FromColumn
	 * @generated
	 */
	public Adapter createFromColumnAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.ToColumn <em>To Column</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.ToColumn
	 * @generated
	 */
	public Adapter createToColumnAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.ReferenceColumn <em>Reference Column</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.ReferenceColumn
	 * @generated
	 */
	public Adapter createReferenceColumnAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.MacroColumn <em>Macro Column</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.MacroColumn
	 * @generated
	 */
	public Adapter createMacroColumnAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.ConnectionURI <em>Connection URI</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.ConnectionURI
	 * @generated
	 */
	public Adapter createConnectionURIAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.MigrationSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.MigrationSource
	 * @generated
	 */
	public Adapter createMigrationSourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link migration.MigrationDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see migration.MigrationDestination
	 * @generated
	 */
	public Adapter createMigrationDestinationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //MigrationAdapterFactory
