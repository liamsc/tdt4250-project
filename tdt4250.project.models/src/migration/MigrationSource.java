/**
 */
package migration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.MigrationSource#getFrom <em>From</em>}</li>
 *   <li>{@link migration.MigrationSource#getConnectionUri <em>Connection Uri</em>}</li>
 *   <li>{@link migration.MigrationSource#getMigration <em>Migration</em>}</li>
 *   <li>{@link migration.MigrationSource#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getMigrationSource()
 * @model
 * @generated
 */
public interface MigrationSource extends EObject {
	/**
	 * Returns the value of the '<em><b>From</b></em>' containment reference list.
	 * The list contents are of type {@link migration.FromTable}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' containment reference list.
	 * @see migration.MigrationPackage#getMigrationSource_From()
	 * @model containment="true"
	 * @generated
	 */
	EList<FromTable> getFrom();

	/**
	 * Returns the value of the '<em><b>Connection Uri</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection Uri</em>' containment reference.
	 * @see #setConnectionUri(ConnectionURI)
	 * @see migration.MigrationPackage#getMigrationSource_ConnectionUri()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ConnectionURI getConnectionUri();

	/**
	 * Sets the value of the '{@link migration.MigrationSource#getConnectionUri <em>Connection Uri</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connection Uri</em>' containment reference.
	 * @see #getConnectionUri()
	 * @generated
	 */
	void setConnectionUri(ConnectionURI value);

	/**
	 * Returns the value of the '<em><b>Migration</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link migration.Migration#getSource2 <em>Source2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Migration</em>' container reference.
	 * @see #setMigration(Migration)
	 * @see migration.MigrationPackage#getMigrationSource_Migration()
	 * @see migration.Migration#getSource2
	 * @model opposite="source2" required="true" transient="false"
	 * @generated
	 */
	Migration getMigration();

	/**
	 * Sets the value of the '{@link migration.MigrationSource#getMigration <em>Migration</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Migration</em>' container reference.
	 * @see #getMigration()
	 * @generated
	 */
	void setMigration(Migration value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see migration.MigrationPackage#getMigrationSource_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link migration.MigrationSource#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // MigrationSource
