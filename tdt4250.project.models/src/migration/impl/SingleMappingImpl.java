/**
 */
package migration.impl;

import migration.FromTable;
import migration.MigrationPackage;
import migration.SingleMapping;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Single Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link migration.impl.SingleMappingImpl#getFromTable <em>From Table</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SingleMappingImpl extends MappingImpl implements SingleMapping {
	/**
	 * The cached value of the '{@link #getFromTable() <em>From Table</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromTable()
	 * @generated
	 * @ordered
	 */
	protected FromTable fromTable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SingleMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MigrationPackage.Literals.SINGLE_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromTable getFromTable() {
		if (fromTable != null && fromTable.eIsProxy()) {
			InternalEObject oldFromTable = (InternalEObject)fromTable;
			fromTable = (FromTable)eResolveProxy(oldFromTable);
			if (fromTable != oldFromTable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MigrationPackage.SINGLE_MAPPING__FROM_TABLE, oldFromTable, fromTable));
			}
		}
		return fromTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromTable basicGetFromTable() {
		return fromTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromTable(FromTable newFromTable) {
		FromTable oldFromTable = fromTable;
		fromTable = newFromTable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.SINGLE_MAPPING__FROM_TABLE, oldFromTable, fromTable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MigrationPackage.SINGLE_MAPPING__FROM_TABLE:
				if (resolve) return getFromTable();
				return basicGetFromTable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MigrationPackage.SINGLE_MAPPING__FROM_TABLE:
				setFromTable((FromTable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MigrationPackage.SINGLE_MAPPING__FROM_TABLE:
				setFromTable((FromTable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MigrationPackage.SINGLE_MAPPING__FROM_TABLE:
				return fromTable != null;
		}
		return super.eIsSet(featureID);
	}

} //SingleMappingImpl
