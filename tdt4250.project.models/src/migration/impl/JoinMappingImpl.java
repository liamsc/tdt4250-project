/**
 */
package migration.impl;

import migration.FromColumn;
import migration.JoinMapping;
import migration.MigrationPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Join Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link migration.impl.JoinMappingImpl#getJoinFieldA <em>Join Field A</em>}</li>
 *   <li>{@link migration.impl.JoinMappingImpl#getJoinFieldB <em>Join Field B</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JoinMappingImpl extends MappingImpl implements JoinMapping {
	/**
	 * The cached value of the '{@link #getJoinFieldA() <em>Join Field A</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJoinFieldA()
	 * @generated
	 * @ordered
	 */
	protected FromColumn joinFieldA;

	/**
	 * The cached value of the '{@link #getJoinFieldB() <em>Join Field B</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJoinFieldB()
	 * @generated
	 * @ordered
	 */
	protected FromColumn joinFieldB;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JoinMappingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MigrationPackage.Literals.JOIN_MAPPING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromColumn getJoinFieldA() {
		if (joinFieldA != null && joinFieldA.eIsProxy()) {
			InternalEObject oldJoinFieldA = (InternalEObject)joinFieldA;
			joinFieldA = (FromColumn)eResolveProxy(oldJoinFieldA);
			if (joinFieldA != oldJoinFieldA) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MigrationPackage.JOIN_MAPPING__JOIN_FIELD_A, oldJoinFieldA, joinFieldA));
			}
		}
		return joinFieldA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromColumn basicGetJoinFieldA() {
		return joinFieldA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJoinFieldA(FromColumn newJoinFieldA) {
		FromColumn oldJoinFieldA = joinFieldA;
		joinFieldA = newJoinFieldA;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.JOIN_MAPPING__JOIN_FIELD_A, oldJoinFieldA, joinFieldA));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromColumn getJoinFieldB() {
		if (joinFieldB != null && joinFieldB.eIsProxy()) {
			InternalEObject oldJoinFieldB = (InternalEObject)joinFieldB;
			joinFieldB = (FromColumn)eResolveProxy(oldJoinFieldB);
			if (joinFieldB != oldJoinFieldB) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MigrationPackage.JOIN_MAPPING__JOIN_FIELD_B, oldJoinFieldB, joinFieldB));
			}
		}
		return joinFieldB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromColumn basicGetJoinFieldB() {
		return joinFieldB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJoinFieldB(FromColumn newJoinFieldB) {
		FromColumn oldJoinFieldB = joinFieldB;
		joinFieldB = newJoinFieldB;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.JOIN_MAPPING__JOIN_FIELD_B, oldJoinFieldB, joinFieldB));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MigrationPackage.JOIN_MAPPING__JOIN_FIELD_A:
				if (resolve) return getJoinFieldA();
				return basicGetJoinFieldA();
			case MigrationPackage.JOIN_MAPPING__JOIN_FIELD_B:
				if (resolve) return getJoinFieldB();
				return basicGetJoinFieldB();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MigrationPackage.JOIN_MAPPING__JOIN_FIELD_A:
				setJoinFieldA((FromColumn)newValue);
				return;
			case MigrationPackage.JOIN_MAPPING__JOIN_FIELD_B:
				setJoinFieldB((FromColumn)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MigrationPackage.JOIN_MAPPING__JOIN_FIELD_A:
				setJoinFieldA((FromColumn)null);
				return;
			case MigrationPackage.JOIN_MAPPING__JOIN_FIELD_B:
				setJoinFieldB((FromColumn)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MigrationPackage.JOIN_MAPPING__JOIN_FIELD_A:
				return joinFieldA != null;
			case MigrationPackage.JOIN_MAPPING__JOIN_FIELD_B:
				return joinFieldB != null;
		}
		return super.eIsSet(featureID);
	}

} //JoinMappingImpl
