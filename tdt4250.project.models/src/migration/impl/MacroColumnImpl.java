/**
 */
package migration.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import migration.FromColumn;
import migration.MacroColumn;
import migration.MigrationPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.xbase.XExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Macro Column</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link migration.impl.MacroColumnImpl#getParams <em>Params</em>}</li>
 *   <li>{@link migration.impl.MacroColumnImpl#getMacroName <em>Macro Name</em>}</li>
 *   <li>{@link migration.impl.MacroColumnImpl#getMacroBody <em>Macro Body</em>}</li>
 *   <li>{@link migration.impl.MacroColumnImpl#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link migration.impl.MacroColumnImpl#getColumns <em>Columns</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MacroColumnImpl extends ToColumnImpl implements MacroColumn {
	/**
	 * The cached value of the '{@link #getParams() <em>Params</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParams()
	 * @generated
	 * @ordered
	 */
	protected EList<JvmFormalParameter> params;

	/**
	 * The default value of the '{@link #getMacroName() <em>Macro Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMacroName()
	 * @generated
	 * @ordered
	 */
	protected static final String MACRO_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMacroName() <em>Macro Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMacroName()
	 * @generated
	 * @ordered
	 */
	protected String macroName = MACRO_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMacroBody() <em>Macro Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMacroBody()
	 * @generated
	 * @ordered
	 */
	protected XExpression macroBody;

	/**
	 * The cached value of the '{@link #getReturnType() <em>Return Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnType()
	 * @generated
	 * @ordered
	 */
	protected JvmTypeReference returnType;

	/**
	 * The cached value of the '{@link #getColumns() <em>Columns</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumns()
	 * @generated
	 * @ordered
	 */
	protected EList<FromColumn> columns;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MacroColumnImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MigrationPackage.Literals.MACRO_COLUMN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JvmFormalParameter> getParams() {
		if (params == null) {
			params = new EObjectContainmentEList<JvmFormalParameter>(JvmFormalParameter.class, this, MigrationPackage.MACRO_COLUMN__PARAMS);
		}
		return params;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMacroName() {
		return macroName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMacroName(String newMacroName) {
		String oldMacroName = macroName;
		macroName = newMacroName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.MACRO_COLUMN__MACRO_NAME, oldMacroName, macroName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XExpression getMacroBody() {
		return macroBody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMacroBody(XExpression newMacroBody, NotificationChain msgs) {
		XExpression oldMacroBody = macroBody;
		macroBody = newMacroBody;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MigrationPackage.MACRO_COLUMN__MACRO_BODY, oldMacroBody, newMacroBody);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMacroBody(XExpression newMacroBody) {
		if (newMacroBody != macroBody) {
			NotificationChain msgs = null;
			if (macroBody != null)
				msgs = ((InternalEObject)macroBody).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MigrationPackage.MACRO_COLUMN__MACRO_BODY, null, msgs);
			if (newMacroBody != null)
				msgs = ((InternalEObject)newMacroBody).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MigrationPackage.MACRO_COLUMN__MACRO_BODY, null, msgs);
			msgs = basicSetMacroBody(newMacroBody, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.MACRO_COLUMN__MACRO_BODY, newMacroBody, newMacroBody));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JvmTypeReference getReturnType() {
		return returnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReturnType(JvmTypeReference newReturnType, NotificationChain msgs) {
		JvmTypeReference oldReturnType = returnType;
		returnType = newReturnType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MigrationPackage.MACRO_COLUMN__RETURN_TYPE, oldReturnType, newReturnType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnType(JvmTypeReference newReturnType) {
		if (newReturnType != returnType) {
			NotificationChain msgs = null;
			if (returnType != null)
				msgs = ((InternalEObject)returnType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MigrationPackage.MACRO_COLUMN__RETURN_TYPE, null, msgs);
			if (newReturnType != null)
				msgs = ((InternalEObject)newReturnType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MigrationPackage.MACRO_COLUMN__RETURN_TYPE, null, msgs);
			msgs = basicSetReturnType(newReturnType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.MACRO_COLUMN__RETURN_TYPE, newReturnType, newReturnType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FromColumn> getColumns() {
		if (columns == null) {
			columns = new EObjectResolvingEList<FromColumn>(FromColumn.class, this, MigrationPackage.MACRO_COLUMN__COLUMNS);
		}
		return columns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MigrationPackage.MACRO_COLUMN__PARAMS:
				return ((InternalEList<?>)getParams()).basicRemove(otherEnd, msgs);
			case MigrationPackage.MACRO_COLUMN__MACRO_BODY:
				return basicSetMacroBody(null, msgs);
			case MigrationPackage.MACRO_COLUMN__RETURN_TYPE:
				return basicSetReturnType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MigrationPackage.MACRO_COLUMN__PARAMS:
				return getParams();
			case MigrationPackage.MACRO_COLUMN__MACRO_NAME:
				return getMacroName();
			case MigrationPackage.MACRO_COLUMN__MACRO_BODY:
				return getMacroBody();
			case MigrationPackage.MACRO_COLUMN__RETURN_TYPE:
				return getReturnType();
			case MigrationPackage.MACRO_COLUMN__COLUMNS:
				return getColumns();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MigrationPackage.MACRO_COLUMN__PARAMS:
				getParams().clear();
				getParams().addAll((Collection<? extends JvmFormalParameter>)newValue);
				return;
			case MigrationPackage.MACRO_COLUMN__MACRO_NAME:
				setMacroName((String)newValue);
				return;
			case MigrationPackage.MACRO_COLUMN__MACRO_BODY:
				setMacroBody((XExpression)newValue);
				return;
			case MigrationPackage.MACRO_COLUMN__RETURN_TYPE:
				setReturnType((JvmTypeReference)newValue);
				return;
			case MigrationPackage.MACRO_COLUMN__COLUMNS:
				getColumns().clear();
				getColumns().addAll((Collection<? extends FromColumn>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MigrationPackage.MACRO_COLUMN__PARAMS:
				getParams().clear();
				return;
			case MigrationPackage.MACRO_COLUMN__MACRO_NAME:
				setMacroName(MACRO_NAME_EDEFAULT);
				return;
			case MigrationPackage.MACRO_COLUMN__MACRO_BODY:
				setMacroBody((XExpression)null);
				return;
			case MigrationPackage.MACRO_COLUMN__RETURN_TYPE:
				setReturnType((JvmTypeReference)null);
				return;
			case MigrationPackage.MACRO_COLUMN__COLUMNS:
				getColumns().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MigrationPackage.MACRO_COLUMN__PARAMS:
				return params != null && !params.isEmpty();
			case MigrationPackage.MACRO_COLUMN__MACRO_NAME:
				return MACRO_NAME_EDEFAULT == null ? macroName != null : !MACRO_NAME_EDEFAULT.equals(macroName);
			case MigrationPackage.MACRO_COLUMN__MACRO_BODY:
				return macroBody != null;
			case MigrationPackage.MACRO_COLUMN__RETURN_TYPE:
				return returnType != null;
			case MigrationPackage.MACRO_COLUMN__COLUMNS:
				return columns != null && !columns.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MigrationPackage.MACRO_COLUMN___GET_NAME:
				return getName();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (macroName: ");
		result.append(macroName);
		result.append(')');
		return result.toString();
	}

} //MacroColumnImpl
