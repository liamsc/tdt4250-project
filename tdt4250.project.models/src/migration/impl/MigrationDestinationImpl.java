/**
 */
package migration.impl;

import java.util.Collection;

import migration.ConnectionURI;
import migration.Migration;
import migration.MigrationDestination;
import migration.MigrationPackage;
import migration.ToTable;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Destination</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link migration.impl.MigrationDestinationImpl#getTo <em>To</em>}</li>
 *   <li>{@link migration.impl.MigrationDestinationImpl#getConnectionUri <em>Connection Uri</em>}</li>
 *   <li>{@link migration.impl.MigrationDestinationImpl#getMigration <em>Migration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MigrationDestinationImpl extends MinimalEObjectImpl.Container implements MigrationDestination {
	/**
	 * The cached value of the '{@link #getTo() <em>To</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected EList<ToTable> to;

	/**
	 * The cached value of the '{@link #getConnectionUri() <em>Connection Uri</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectionUri()
	 * @generated
	 * @ordered
	 */
	protected ConnectionURI connectionUri;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MigrationDestinationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MigrationPackage.Literals.MIGRATION_DESTINATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ToTable> getTo() {
		if (to == null) {
			to = new EObjectContainmentWithInverseEList<ToTable>(ToTable.class, this, MigrationPackage.MIGRATION_DESTINATION__TO, MigrationPackage.TO_TABLE__MIGRATION);
		}
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionURI getConnectionUri() {
		return connectionUri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConnectionUri(ConnectionURI newConnectionUri, NotificationChain msgs) {
		ConnectionURI oldConnectionUri = connectionUri;
		connectionUri = newConnectionUri;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MigrationPackage.MIGRATION_DESTINATION__CONNECTION_URI, oldConnectionUri, newConnectionUri);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnectionUri(ConnectionURI newConnectionUri) {
		if (newConnectionUri != connectionUri) {
			NotificationChain msgs = null;
			if (connectionUri != null)
				msgs = ((InternalEObject)connectionUri).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MigrationPackage.MIGRATION_DESTINATION__CONNECTION_URI, null, msgs);
			if (newConnectionUri != null)
				msgs = ((InternalEObject)newConnectionUri).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MigrationPackage.MIGRATION_DESTINATION__CONNECTION_URI, null, msgs);
			msgs = basicSetConnectionUri(newConnectionUri, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.MIGRATION_DESTINATION__CONNECTION_URI, newConnectionUri, newConnectionUri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Migration getMigration() {
		if (eContainerFeatureID() != MigrationPackage.MIGRATION_DESTINATION__MIGRATION) return null;
		return (Migration)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMigration(Migration newMigration, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newMigration, MigrationPackage.MIGRATION_DESTINATION__MIGRATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMigration(Migration newMigration) {
		if (newMigration != eInternalContainer() || (eContainerFeatureID() != MigrationPackage.MIGRATION_DESTINATION__MIGRATION && newMigration != null)) {
			if (EcoreUtil.isAncestor(this, newMigration))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMigration != null)
				msgs = ((InternalEObject)newMigration).eInverseAdd(this, MigrationPackage.MIGRATION__DESTINATION, Migration.class, msgs);
			msgs = basicSetMigration(newMigration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.MIGRATION_DESTINATION__MIGRATION, newMigration, newMigration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_DESTINATION__TO:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTo()).basicAdd(otherEnd, msgs);
			case MigrationPackage.MIGRATION_DESTINATION__MIGRATION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetMigration((Migration)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_DESTINATION__TO:
				return ((InternalEList<?>)getTo()).basicRemove(otherEnd, msgs);
			case MigrationPackage.MIGRATION_DESTINATION__CONNECTION_URI:
				return basicSetConnectionUri(null, msgs);
			case MigrationPackage.MIGRATION_DESTINATION__MIGRATION:
				return basicSetMigration(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case MigrationPackage.MIGRATION_DESTINATION__MIGRATION:
				return eInternalContainer().eInverseRemove(this, MigrationPackage.MIGRATION__DESTINATION, Migration.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_DESTINATION__TO:
				return getTo();
			case MigrationPackage.MIGRATION_DESTINATION__CONNECTION_URI:
				return getConnectionUri();
			case MigrationPackage.MIGRATION_DESTINATION__MIGRATION:
				return getMigration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_DESTINATION__TO:
				getTo().clear();
				getTo().addAll((Collection<? extends ToTable>)newValue);
				return;
			case MigrationPackage.MIGRATION_DESTINATION__CONNECTION_URI:
				setConnectionUri((ConnectionURI)newValue);
				return;
			case MigrationPackage.MIGRATION_DESTINATION__MIGRATION:
				setMigration((Migration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_DESTINATION__TO:
				getTo().clear();
				return;
			case MigrationPackage.MIGRATION_DESTINATION__CONNECTION_URI:
				setConnectionUri((ConnectionURI)null);
				return;
			case MigrationPackage.MIGRATION_DESTINATION__MIGRATION:
				setMigration((Migration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_DESTINATION__TO:
				return to != null && !to.isEmpty();
			case MigrationPackage.MIGRATION_DESTINATION__CONNECTION_URI:
				return connectionUri != null;
			case MigrationPackage.MIGRATION_DESTINATION__MIGRATION:
				return getMigration() != null;
		}
		return super.eIsSet(featureID);
	}

} //MigrationDestinationImpl
