/**
 */
package migration.impl;

import java.util.Collection;

import migration.Mapping;
import migration.MigrationDestination;
import migration.MigrationPackage;
import migration.ToColumn;
import migration.ToTable;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>To Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link migration.impl.ToTableImpl#getMapping <em>Mapping</em>}</li>
 *   <li>{@link migration.impl.ToTableImpl#getName <em>Name</em>}</li>
 *   <li>{@link migration.impl.ToTableImpl#getFields <em>Fields</em>}</li>
 *   <li>{@link migration.impl.ToTableImpl#getMigration <em>Migration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ToTableImpl extends MinimalEObjectImpl.Container implements ToTable {
	/**
	 * The cached value of the '{@link #getMapping() <em>Mapping</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMapping()
	 * @generated
	 * @ordered
	 */
	protected Mapping mapping;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFields() <em>Fields</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFields()
	 * @generated
	 * @ordered
	 */
	protected EList<ToColumn> fields;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ToTableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MigrationPackage.Literals.TO_TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mapping getMapping() {
		return mapping;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMapping(Mapping newMapping, NotificationChain msgs) {
		Mapping oldMapping = mapping;
		mapping = newMapping;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MigrationPackage.TO_TABLE__MAPPING, oldMapping, newMapping);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMapping(Mapping newMapping) {
		if (newMapping != mapping) {
			NotificationChain msgs = null;
			if (mapping != null)
				msgs = ((InternalEObject)mapping).eInverseRemove(this, MigrationPackage.MAPPING__TABLE, Mapping.class, msgs);
			if (newMapping != null)
				msgs = ((InternalEObject)newMapping).eInverseAdd(this, MigrationPackage.MAPPING__TABLE, Mapping.class, msgs);
			msgs = basicSetMapping(newMapping, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.TO_TABLE__MAPPING, newMapping, newMapping));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.TO_TABLE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ToColumn> getFields() {
		if (fields == null) {
			fields = new EObjectContainmentWithInverseEList<ToColumn>(ToColumn.class, this, MigrationPackage.TO_TABLE__FIELDS, MigrationPackage.TO_COLUMN__TABLE);
		}
		return fields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MigrationDestination getMigration() {
		if (eContainerFeatureID() != MigrationPackage.TO_TABLE__MIGRATION) return null;
		return (MigrationDestination)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMigration(MigrationDestination newMigration, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newMigration, MigrationPackage.TO_TABLE__MIGRATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMigration(MigrationDestination newMigration) {
		if (newMigration != eInternalContainer() || (eContainerFeatureID() != MigrationPackage.TO_TABLE__MIGRATION && newMigration != null)) {
			if (EcoreUtil.isAncestor(this, newMigration))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMigration != null)
				msgs = ((InternalEObject)newMigration).eInverseAdd(this, MigrationPackage.MIGRATION_DESTINATION__TO, MigrationDestination.class, msgs);
			msgs = basicSetMigration(newMigration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.TO_TABLE__MIGRATION, newMigration, newMigration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MigrationPackage.TO_TABLE__MAPPING:
				if (mapping != null)
					msgs = ((InternalEObject)mapping).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MigrationPackage.TO_TABLE__MAPPING, null, msgs);
				return basicSetMapping((Mapping)otherEnd, msgs);
			case MigrationPackage.TO_TABLE__FIELDS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFields()).basicAdd(otherEnd, msgs);
			case MigrationPackage.TO_TABLE__MIGRATION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetMigration((MigrationDestination)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MigrationPackage.TO_TABLE__MAPPING:
				return basicSetMapping(null, msgs);
			case MigrationPackage.TO_TABLE__FIELDS:
				return ((InternalEList<?>)getFields()).basicRemove(otherEnd, msgs);
			case MigrationPackage.TO_TABLE__MIGRATION:
				return basicSetMigration(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case MigrationPackage.TO_TABLE__MIGRATION:
				return eInternalContainer().eInverseRemove(this, MigrationPackage.MIGRATION_DESTINATION__TO, MigrationDestination.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MigrationPackage.TO_TABLE__MAPPING:
				return getMapping();
			case MigrationPackage.TO_TABLE__NAME:
				return getName();
			case MigrationPackage.TO_TABLE__FIELDS:
				return getFields();
			case MigrationPackage.TO_TABLE__MIGRATION:
				return getMigration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MigrationPackage.TO_TABLE__MAPPING:
				setMapping((Mapping)newValue);
				return;
			case MigrationPackage.TO_TABLE__NAME:
				setName((String)newValue);
				return;
			case MigrationPackage.TO_TABLE__FIELDS:
				getFields().clear();
				getFields().addAll((Collection<? extends ToColumn>)newValue);
				return;
			case MigrationPackage.TO_TABLE__MIGRATION:
				setMigration((MigrationDestination)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MigrationPackage.TO_TABLE__MAPPING:
				setMapping((Mapping)null);
				return;
			case MigrationPackage.TO_TABLE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MigrationPackage.TO_TABLE__FIELDS:
				getFields().clear();
				return;
			case MigrationPackage.TO_TABLE__MIGRATION:
				setMigration((MigrationDestination)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MigrationPackage.TO_TABLE__MAPPING:
				return mapping != null;
			case MigrationPackage.TO_TABLE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MigrationPackage.TO_TABLE__FIELDS:
				return fields != null && !fields.isEmpty();
			case MigrationPackage.TO_TABLE__MIGRATION:
				return getMigration() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ToTableImpl
