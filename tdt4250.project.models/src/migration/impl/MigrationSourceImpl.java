/**
 */
package migration.impl;

import java.util.Collection;

import migration.ConnectionURI;
import migration.FromTable;
import migration.Migration;
import migration.MigrationPackage;
import migration.MigrationSource;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Source</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link migration.impl.MigrationSourceImpl#getFrom <em>From</em>}</li>
 *   <li>{@link migration.impl.MigrationSourceImpl#getConnectionUri <em>Connection Uri</em>}</li>
 *   <li>{@link migration.impl.MigrationSourceImpl#getMigration <em>Migration</em>}</li>
 *   <li>{@link migration.impl.MigrationSourceImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MigrationSourceImpl extends MinimalEObjectImpl.Container implements MigrationSource {
	/**
	 * The cached value of the '{@link #getFrom() <em>From</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected EList<FromTable> from;

	/**
	 * The cached value of the '{@link #getConnectionUri() <em>Connection Uri</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectionUri()
	 * @generated
	 * @ordered
	 */
	protected ConnectionURI connectionUri;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MigrationSourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MigrationPackage.Literals.MIGRATION_SOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FromTable> getFrom() {
		if (from == null) {
			from = new EObjectContainmentEList<FromTable>(FromTable.class, this, MigrationPackage.MIGRATION_SOURCE__FROM);
		}
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionURI getConnectionUri() {
		return connectionUri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConnectionUri(ConnectionURI newConnectionUri, NotificationChain msgs) {
		ConnectionURI oldConnectionUri = connectionUri;
		connectionUri = newConnectionUri;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MigrationPackage.MIGRATION_SOURCE__CONNECTION_URI, oldConnectionUri, newConnectionUri);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnectionUri(ConnectionURI newConnectionUri) {
		if (newConnectionUri != connectionUri) {
			NotificationChain msgs = null;
			if (connectionUri != null)
				msgs = ((InternalEObject)connectionUri).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MigrationPackage.MIGRATION_SOURCE__CONNECTION_URI, null, msgs);
			if (newConnectionUri != null)
				msgs = ((InternalEObject)newConnectionUri).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MigrationPackage.MIGRATION_SOURCE__CONNECTION_URI, null, msgs);
			msgs = basicSetConnectionUri(newConnectionUri, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.MIGRATION_SOURCE__CONNECTION_URI, newConnectionUri, newConnectionUri));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Migration getMigration() {
		if (eContainerFeatureID() != MigrationPackage.MIGRATION_SOURCE__MIGRATION) return null;
		return (Migration)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMigration(Migration newMigration, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newMigration, MigrationPackage.MIGRATION_SOURCE__MIGRATION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMigration(Migration newMigration) {
		if (newMigration != eInternalContainer() || (eContainerFeatureID() != MigrationPackage.MIGRATION_SOURCE__MIGRATION && newMigration != null)) {
			if (EcoreUtil.isAncestor(this, newMigration))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newMigration != null)
				msgs = ((InternalEObject)newMigration).eInverseAdd(this, MigrationPackage.MIGRATION__SOURCE2, Migration.class, msgs);
			msgs = basicSetMigration(newMigration, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.MIGRATION_SOURCE__MIGRATION, newMigration, newMigration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MigrationPackage.MIGRATION_SOURCE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_SOURCE__MIGRATION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetMigration((Migration)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_SOURCE__FROM:
				return ((InternalEList<?>)getFrom()).basicRemove(otherEnd, msgs);
			case MigrationPackage.MIGRATION_SOURCE__CONNECTION_URI:
				return basicSetConnectionUri(null, msgs);
			case MigrationPackage.MIGRATION_SOURCE__MIGRATION:
				return basicSetMigration(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case MigrationPackage.MIGRATION_SOURCE__MIGRATION:
				return eInternalContainer().eInverseRemove(this, MigrationPackage.MIGRATION__SOURCE2, Migration.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_SOURCE__FROM:
				return getFrom();
			case MigrationPackage.MIGRATION_SOURCE__CONNECTION_URI:
				return getConnectionUri();
			case MigrationPackage.MIGRATION_SOURCE__MIGRATION:
				return getMigration();
			case MigrationPackage.MIGRATION_SOURCE__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_SOURCE__FROM:
				getFrom().clear();
				getFrom().addAll((Collection<? extends FromTable>)newValue);
				return;
			case MigrationPackage.MIGRATION_SOURCE__CONNECTION_URI:
				setConnectionUri((ConnectionURI)newValue);
				return;
			case MigrationPackage.MIGRATION_SOURCE__MIGRATION:
				setMigration((Migration)newValue);
				return;
			case MigrationPackage.MIGRATION_SOURCE__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_SOURCE__FROM:
				getFrom().clear();
				return;
			case MigrationPackage.MIGRATION_SOURCE__CONNECTION_URI:
				setConnectionUri((ConnectionURI)null);
				return;
			case MigrationPackage.MIGRATION_SOURCE__MIGRATION:
				setMigration((Migration)null);
				return;
			case MigrationPackage.MIGRATION_SOURCE__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MigrationPackage.MIGRATION_SOURCE__FROM:
				return from != null && !from.isEmpty();
			case MigrationPackage.MIGRATION_SOURCE__CONNECTION_URI:
				return connectionUri != null;
			case MigrationPackage.MIGRATION_SOURCE__MIGRATION:
				return getMigration() != null;
			case MigrationPackage.MIGRATION_SOURCE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //MigrationSourceImpl
