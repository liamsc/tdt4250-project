/**
 */
package migration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>To Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.ToTable#getMapping <em>Mapping</em>}</li>
 *   <li>{@link migration.ToTable#getName <em>Name</em>}</li>
 *   <li>{@link migration.ToTable#getFields <em>Fields</em>}</li>
 *   <li>{@link migration.ToTable#getMigration <em>Migration</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getToTable()
 * @model
 * @generated
 */
public interface ToTable extends EObject {
	/**
	 * Returns the value of the '<em><b>Mapping</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link migration.Mapping#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mapping</em>' containment reference.
	 * @see #setMapping(Mapping)
	 * @see migration.MigrationPackage#getToTable_Mapping()
	 * @see migration.Mapping#getTable
	 * @model opposite="table" containment="true" required="true"
	 * @generated
	 */
	Mapping getMapping();

	/**
	 * Sets the value of the '{@link migration.ToTable#getMapping <em>Mapping</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mapping</em>' containment reference.
	 * @see #getMapping()
	 * @generated
	 */
	void setMapping(Mapping value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see migration.MigrationPackage#getToTable_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link migration.ToTable#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
	 * The list contents are of type {@link migration.ToColumn}.
	 * It is bidirectional and its opposite is '{@link migration.ToColumn#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields</em>' containment reference list.
	 * @see migration.MigrationPackage#getToTable_Fields()
	 * @see migration.ToColumn#getTable
	 * @model opposite="table" containment="true"
	 * @generated
	 */
	EList<ToColumn> getFields();

	/**
	 * Returns the value of the '<em><b>Migration</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link migration.MigrationDestination#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Migration</em>' container reference.
	 * @see #setMigration(MigrationDestination)
	 * @see migration.MigrationPackage#getToTable_Migration()
	 * @see migration.MigrationDestination#getTo
	 * @model opposite="to" required="true" transient="false"
	 * @generated
	 */
	MigrationDestination getMigration();

	/**
	 * Sets the value of the '{@link migration.ToTable#getMigration <em>Migration</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Migration</em>' container reference.
	 * @see #getMigration()
	 * @generated
	 */
	void setMigration(MigrationDestination value);

} // ToTable
