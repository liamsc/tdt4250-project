/**
 */
package migration;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see migration.MigrationFactory
 * @model kind="package"
 * @generated
 */
public interface MigrationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "migration";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/tdt4250.project.models/model/migration.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "tdt4250.databaseMigration";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MigrationPackage eINSTANCE = migration.impl.MigrationPackageImpl.init();

	/**
	 * The meta object id for the '{@link migration.impl.MigrationImpl <em>Migration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.MigrationImpl
	 * @see migration.impl.MigrationPackageImpl#getMigration()
	 * @generated
	 */
	int MIGRATION = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Destination</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION__DESTINATION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION__NAME = 2;

	/**
	 * The feature id for the '<em><b>Source2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION__SOURCE2 = 3;

	/**
	 * The number of structural features of the '<em>Migration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Migration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link migration.impl.FromTableImpl <em>From Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.FromTableImpl
	 * @see migration.impl.MigrationPackageImpl#getFromTable()
	 * @generated
	 */
	int FROM_TABLE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_TABLE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_TABLE__FIELDS = 1;

	/**
	 * The number of structural features of the '<em>From Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_TABLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>From Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_TABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link migration.impl.ToTableImpl <em>To Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.ToTableImpl
	 * @see migration.impl.MigrationPackageImpl#getToTable()
	 * @generated
	 */
	int TO_TABLE = 2;

	/**
	 * The feature id for the '<em><b>Mapping</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_TABLE__MAPPING = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_TABLE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_TABLE__FIELDS = 2;

	/**
	 * The feature id for the '<em><b>Migration</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_TABLE__MIGRATION = 3;

	/**
	 * The number of structural features of the '<em>To Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_TABLE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>To Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_TABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link migration.impl.MappingImpl <em>Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.MappingImpl
	 * @see migration.impl.MigrationPackageImpl#getMapping()
	 * @generated
	 */
	int MAPPING = 3;

	/**
	 * The feature id for the '<em><b>Table</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING__TABLE = 0;

	/**
	 * The number of structural features of the '<em>Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAPPING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link migration.impl.SingleMappingImpl <em>Single Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.SingleMappingImpl
	 * @see migration.impl.MigrationPackageImpl#getSingleMapping()
	 * @generated
	 */
	int SINGLE_MAPPING = 4;

	/**
	 * The feature id for the '<em><b>Table</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_MAPPING__TABLE = MAPPING__TABLE;

	/**
	 * The feature id for the '<em><b>From Table</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_MAPPING__FROM_TABLE = MAPPING_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Single Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_MAPPING_FEATURE_COUNT = MAPPING_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Single Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_MAPPING_OPERATION_COUNT = MAPPING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link migration.impl.JoinMappingImpl <em>Join Mapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.JoinMappingImpl
	 * @see migration.impl.MigrationPackageImpl#getJoinMapping()
	 * @generated
	 */
	int JOIN_MAPPING = 5;

	/**
	 * The feature id for the '<em><b>Table</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_MAPPING__TABLE = MAPPING__TABLE;

	/**
	 * The feature id for the '<em><b>Join Field A</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_MAPPING__JOIN_FIELD_A = MAPPING_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Join Field B</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_MAPPING__JOIN_FIELD_B = MAPPING_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Join Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_MAPPING_FEATURE_COUNT = MAPPING_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Join Mapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JOIN_MAPPING_OPERATION_COUNT = MAPPING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link migration.impl.FromColumnImpl <em>From Column</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.FromColumnImpl
	 * @see migration.impl.MigrationPackageImpl#getFromColumn()
	 * @generated
	 */
	int FROM_COLUMN = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_COLUMN__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_COLUMN__TYPE = 1;

	/**
	 * The feature id for the '<em><b>Table</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_COLUMN__TABLE = 2;

	/**
	 * The number of structural features of the '<em>From Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_COLUMN_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>From Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_COLUMN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link migration.impl.ToColumnImpl <em>To Column</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.ToColumnImpl
	 * @see migration.impl.MigrationPackageImpl#getToColumn()
	 * @generated
	 */
	int TO_COLUMN = 7;

	/**
	 * The feature id for the '<em><b>Column Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_COLUMN__COLUMN_NAME = 0;

	/**
	 * The feature id for the '<em><b>Table</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_COLUMN__TABLE = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_COLUMN__TYPE = 2;

	/**
	 * The number of structural features of the '<em>To Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_COLUMN_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>To Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TO_COLUMN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link migration.impl.ReferenceColumnImpl <em>Reference Column</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.ReferenceColumnImpl
	 * @see migration.impl.MigrationPackageImpl#getReferenceColumn()
	 * @generated
	 */
	int REFERENCE_COLUMN = 8;

	/**
	 * The feature id for the '<em><b>Column Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_COLUMN__COLUMN_NAME = TO_COLUMN__COLUMN_NAME;

	/**
	 * The feature id for the '<em><b>Table</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_COLUMN__TABLE = TO_COLUMN__TABLE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_COLUMN__TYPE = TO_COLUMN__TYPE;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_COLUMN__FROM = TO_COLUMN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_COLUMN_FEATURE_COUNT = TO_COLUMN_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_COLUMN___GET_NAME = TO_COLUMN_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Reference Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_COLUMN_OPERATION_COUNT = TO_COLUMN_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link migration.impl.MacroColumnImpl <em>Macro Column</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.MacroColumnImpl
	 * @see migration.impl.MigrationPackageImpl#getMacroColumn()
	 * @generated
	 */
	int MACRO_COLUMN = 9;

	/**
	 * The feature id for the '<em><b>Column Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN__COLUMN_NAME = TO_COLUMN__COLUMN_NAME;

	/**
	 * The feature id for the '<em><b>Table</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN__TABLE = TO_COLUMN__TABLE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN__TYPE = TO_COLUMN__TYPE;

	/**
	 * The feature id for the '<em><b>Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN__PARAMS = TO_COLUMN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Macro Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN__MACRO_NAME = TO_COLUMN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Macro Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN__MACRO_BODY = TO_COLUMN_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN__RETURN_TYPE = TO_COLUMN_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Columns</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN__COLUMNS = TO_COLUMN_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Macro Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN_FEATURE_COUNT = TO_COLUMN_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN___GET_NAME = TO_COLUMN_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Macro Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MACRO_COLUMN_OPERATION_COUNT = TO_COLUMN_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link migration.impl.ConnectionURIImpl <em>Connection URI</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.ConnectionURIImpl
	 * @see migration.impl.MigrationPackageImpl#getConnectionURI()
	 * @generated
	 */
	int CONNECTION_URI = 10;

	/**
	 * The feature id for the '<em><b>URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_URI__URI = 0;

	/**
	 * The feature id for the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_URI__USERNAME = 1;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_URI__PASSWORD = 2;

	/**
	 * The number of structural features of the '<em>Connection URI</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_URI_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Connection URI</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_URI_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link migration.impl.MigrationSourceImpl <em>Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.MigrationSourceImpl
	 * @see migration.impl.MigrationPackageImpl#getMigrationSource()
	 * @generated
	 */
	int MIGRATION_SOURCE = 11;

	/**
	 * The feature id for the '<em><b>From</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_SOURCE__FROM = 0;

	/**
	 * The feature id for the '<em><b>Connection Uri</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_SOURCE__CONNECTION_URI = 1;

	/**
	 * The feature id for the '<em><b>Migration</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_SOURCE__MIGRATION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_SOURCE__NAME = 3;

	/**
	 * The number of structural features of the '<em>Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_SOURCE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_SOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link migration.impl.MigrationDestinationImpl <em>Destination</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.impl.MigrationDestinationImpl
	 * @see migration.impl.MigrationPackageImpl#getMigrationDestination()
	 * @generated
	 */
	int MIGRATION_DESTINATION = 12;

	/**
	 * The feature id for the '<em><b>To</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_DESTINATION__TO = 0;

	/**
	 * The feature id for the '<em><b>Connection Uri</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_DESTINATION__CONNECTION_URI = 1;

	/**
	 * The feature id for the '<em><b>Migration</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_DESTINATION__MIGRATION = 2;

	/**
	 * The number of structural features of the '<em>Destination</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_DESTINATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Destination</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MIGRATION_DESTINATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link migration.DataType <em>Data Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.DataType
	 * @see migration.impl.MigrationPackageImpl#getDataType()
	 * @generated
	 */
	int DATA_TYPE = 13;


	/**
	 * Returns the meta object for class '{@link migration.Migration <em>Migration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Migration</em>'.
	 * @see migration.Migration
	 * @generated
	 */
	EClass getMigration();

	/**
	 * Returns the meta object for the reference '{@link migration.Migration#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see migration.Migration#getSource()
	 * @see #getMigration()
	 * @generated
	 */
	EReference getMigration_Source();

	/**
	 * Returns the meta object for the containment reference '{@link migration.Migration#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Destination</em>'.
	 * @see migration.Migration#getDestination()
	 * @see #getMigration()
	 * @generated
	 */
	EReference getMigration_Destination();

	/**
	 * Returns the meta object for the attribute '{@link migration.Migration#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see migration.Migration#getName()
	 * @see #getMigration()
	 * @generated
	 */
	EAttribute getMigration_Name();

	/**
	 * Returns the meta object for the containment reference '{@link migration.Migration#getSource2 <em>Source2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source2</em>'.
	 * @see migration.Migration#getSource2()
	 * @see #getMigration()
	 * @generated
	 */
	EReference getMigration_Source2();

	/**
	 * Returns the meta object for class '{@link migration.FromTable <em>From Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>From Table</em>'.
	 * @see migration.FromTable
	 * @generated
	 */
	EClass getFromTable();

	/**
	 * Returns the meta object for the attribute '{@link migration.FromTable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see migration.FromTable#getName()
	 * @see #getFromTable()
	 * @generated
	 */
	EAttribute getFromTable_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link migration.FromTable#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see migration.FromTable#getFields()
	 * @see #getFromTable()
	 * @generated
	 */
	EReference getFromTable_Fields();

	/**
	 * Returns the meta object for class '{@link migration.ToTable <em>To Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>To Table</em>'.
	 * @see migration.ToTable
	 * @generated
	 */
	EClass getToTable();

	/**
	 * Returns the meta object for the containment reference '{@link migration.ToTable#getMapping <em>Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Mapping</em>'.
	 * @see migration.ToTable#getMapping()
	 * @see #getToTable()
	 * @generated
	 */
	EReference getToTable_Mapping();

	/**
	 * Returns the meta object for the attribute '{@link migration.ToTable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see migration.ToTable#getName()
	 * @see #getToTable()
	 * @generated
	 */
	EAttribute getToTable_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link migration.ToTable#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see migration.ToTable#getFields()
	 * @see #getToTable()
	 * @generated
	 */
	EReference getToTable_Fields();

	/**
	 * Returns the meta object for the container reference '{@link migration.ToTable#getMigration <em>Migration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Migration</em>'.
	 * @see migration.ToTable#getMigration()
	 * @see #getToTable()
	 * @generated
	 */
	EReference getToTable_Migration();

	/**
	 * Returns the meta object for class '{@link migration.Mapping <em>Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mapping</em>'.
	 * @see migration.Mapping
	 * @generated
	 */
	EClass getMapping();

	/**
	 * Returns the meta object for the container reference '{@link migration.Mapping#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Table</em>'.
	 * @see migration.Mapping#getTable()
	 * @see #getMapping()
	 * @generated
	 */
	EReference getMapping_Table();

	/**
	 * Returns the meta object for class '{@link migration.SingleMapping <em>Single Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Mapping</em>'.
	 * @see migration.SingleMapping
	 * @generated
	 */
	EClass getSingleMapping();

	/**
	 * Returns the meta object for the reference '{@link migration.SingleMapping#getFromTable <em>From Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From Table</em>'.
	 * @see migration.SingleMapping#getFromTable()
	 * @see #getSingleMapping()
	 * @generated
	 */
	EReference getSingleMapping_FromTable();

	/**
	 * Returns the meta object for class '{@link migration.JoinMapping <em>Join Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Join Mapping</em>'.
	 * @see migration.JoinMapping
	 * @generated
	 */
	EClass getJoinMapping();

	/**
	 * Returns the meta object for the reference '{@link migration.JoinMapping#getJoinFieldA <em>Join Field A</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Join Field A</em>'.
	 * @see migration.JoinMapping#getJoinFieldA()
	 * @see #getJoinMapping()
	 * @generated
	 */
	EReference getJoinMapping_JoinFieldA();

	/**
	 * Returns the meta object for the reference '{@link migration.JoinMapping#getJoinFieldB <em>Join Field B</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Join Field B</em>'.
	 * @see migration.JoinMapping#getJoinFieldB()
	 * @see #getJoinMapping()
	 * @generated
	 */
	EReference getJoinMapping_JoinFieldB();

	/**
	 * Returns the meta object for class '{@link migration.FromColumn <em>From Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>From Column</em>'.
	 * @see migration.FromColumn
	 * @generated
	 */
	EClass getFromColumn();

	/**
	 * Returns the meta object for the attribute '{@link migration.FromColumn#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see migration.FromColumn#getName()
	 * @see #getFromColumn()
	 * @generated
	 */
	EAttribute getFromColumn_Name();

	/**
	 * Returns the meta object for the attribute '{@link migration.FromColumn#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see migration.FromColumn#getType()
	 * @see #getFromColumn()
	 * @generated
	 */
	EAttribute getFromColumn_Type();

	/**
	 * Returns the meta object for the container reference '{@link migration.FromColumn#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Table</em>'.
	 * @see migration.FromColumn#getTable()
	 * @see #getFromColumn()
	 * @generated
	 */
	EReference getFromColumn_Table();

	/**
	 * Returns the meta object for class '{@link migration.ToColumn <em>To Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>To Column</em>'.
	 * @see migration.ToColumn
	 * @generated
	 */
	EClass getToColumn();

	/**
	 * Returns the meta object for the attribute '{@link migration.ToColumn#getColumnName <em>Column Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Column Name</em>'.
	 * @see migration.ToColumn#getColumnName()
	 * @see #getToColumn()
	 * @generated
	 */
	EAttribute getToColumn_ColumnName();

	/**
	 * Returns the meta object for the container reference '{@link migration.ToColumn#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Table</em>'.
	 * @see migration.ToColumn#getTable()
	 * @see #getToColumn()
	 * @generated
	 */
	EReference getToColumn_Table();

	/**
	 * Returns the meta object for the attribute '{@link migration.ToColumn#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see migration.ToColumn#getType()
	 * @see #getToColumn()
	 * @generated
	 */
	EAttribute getToColumn_Type();

	/**
	 * Returns the meta object for class '{@link migration.ReferenceColumn <em>Reference Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Column</em>'.
	 * @see migration.ReferenceColumn
	 * @generated
	 */
	EClass getReferenceColumn();

	/**
	 * Returns the meta object for the reference '{@link migration.ReferenceColumn#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see migration.ReferenceColumn#getFrom()
	 * @see #getReferenceColumn()
	 * @generated
	 */
	EReference getReferenceColumn_From();

	/**
	 * Returns the meta object for the '{@link migration.ReferenceColumn#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see migration.ReferenceColumn#getName()
	 * @generated
	 */
	EOperation getReferenceColumn__GetName();

	/**
	 * Returns the meta object for class '{@link migration.MacroColumn <em>Macro Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Macro Column</em>'.
	 * @see migration.MacroColumn
	 * @generated
	 */
	EClass getMacroColumn();

	/**
	 * Returns the meta object for the containment reference list '{@link migration.MacroColumn#getParams <em>Params</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Params</em>'.
	 * @see migration.MacroColumn#getParams()
	 * @see #getMacroColumn()
	 * @generated
	 */
	EReference getMacroColumn_Params();

	/**
	 * Returns the meta object for the attribute '{@link migration.MacroColumn#getMacroName <em>Macro Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Macro Name</em>'.
	 * @see migration.MacroColumn#getMacroName()
	 * @see #getMacroColumn()
	 * @generated
	 */
	EAttribute getMacroColumn_MacroName();

	/**
	 * Returns the meta object for the containment reference '{@link migration.MacroColumn#getMacroBody <em>Macro Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Macro Body</em>'.
	 * @see migration.MacroColumn#getMacroBody()
	 * @see #getMacroColumn()
	 * @generated
	 */
	EReference getMacroColumn_MacroBody();

	/**
	 * Returns the meta object for the containment reference '{@link migration.MacroColumn#getReturnType <em>Return Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Return Type</em>'.
	 * @see migration.MacroColumn#getReturnType()
	 * @see #getMacroColumn()
	 * @generated
	 */
	EReference getMacroColumn_ReturnType();

	/**
	 * Returns the meta object for the reference list '{@link migration.MacroColumn#getColumns <em>Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Columns</em>'.
	 * @see migration.MacroColumn#getColumns()
	 * @see #getMacroColumn()
	 * @generated
	 */
	EReference getMacroColumn_Columns();

	/**
	 * Returns the meta object for the '{@link migration.MacroColumn#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see migration.MacroColumn#getName()
	 * @generated
	 */
	EOperation getMacroColumn__GetName();

	/**
	 * Returns the meta object for class '{@link migration.ConnectionURI <em>Connection URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection URI</em>'.
	 * @see migration.ConnectionURI
	 * @generated
	 */
	EClass getConnectionURI();

	/**
	 * Returns the meta object for the attribute '{@link migration.ConnectionURI#getURI <em>URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>URI</em>'.
	 * @see migration.ConnectionURI#getURI()
	 * @see #getConnectionURI()
	 * @generated
	 */
	EAttribute getConnectionURI_URI();

	/**
	 * Returns the meta object for the attribute '{@link migration.ConnectionURI#getUsername <em>Username</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Username</em>'.
	 * @see migration.ConnectionURI#getUsername()
	 * @see #getConnectionURI()
	 * @generated
	 */
	EAttribute getConnectionURI_Username();

	/**
	 * Returns the meta object for the attribute '{@link migration.ConnectionURI#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see migration.ConnectionURI#getPassword()
	 * @see #getConnectionURI()
	 * @generated
	 */
	EAttribute getConnectionURI_Password();

	/**
	 * Returns the meta object for class '{@link migration.MigrationSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source</em>'.
	 * @see migration.MigrationSource
	 * @generated
	 */
	EClass getMigrationSource();

	/**
	 * Returns the meta object for the containment reference list '{@link migration.MigrationSource#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>From</em>'.
	 * @see migration.MigrationSource#getFrom()
	 * @see #getMigrationSource()
	 * @generated
	 */
	EReference getMigrationSource_From();

	/**
	 * Returns the meta object for the containment reference '{@link migration.MigrationSource#getConnectionUri <em>Connection Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Connection Uri</em>'.
	 * @see migration.MigrationSource#getConnectionUri()
	 * @see #getMigrationSource()
	 * @generated
	 */
	EReference getMigrationSource_ConnectionUri();

	/**
	 * Returns the meta object for the container reference '{@link migration.MigrationSource#getMigration <em>Migration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Migration</em>'.
	 * @see migration.MigrationSource#getMigration()
	 * @see #getMigrationSource()
	 * @generated
	 */
	EReference getMigrationSource_Migration();

	/**
	 * Returns the meta object for the attribute '{@link migration.MigrationSource#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see migration.MigrationSource#getName()
	 * @see #getMigrationSource()
	 * @generated
	 */
	EAttribute getMigrationSource_Name();

	/**
	 * Returns the meta object for class '{@link migration.MigrationDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Destination</em>'.
	 * @see migration.MigrationDestination
	 * @generated
	 */
	EClass getMigrationDestination();

	/**
	 * Returns the meta object for the containment reference list '{@link migration.MigrationDestination#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>To</em>'.
	 * @see migration.MigrationDestination#getTo()
	 * @see #getMigrationDestination()
	 * @generated
	 */
	EReference getMigrationDestination_To();

	/**
	 * Returns the meta object for the containment reference '{@link migration.MigrationDestination#getConnectionUri <em>Connection Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Connection Uri</em>'.
	 * @see migration.MigrationDestination#getConnectionUri()
	 * @see #getMigrationDestination()
	 * @generated
	 */
	EReference getMigrationDestination_ConnectionUri();

	/**
	 * Returns the meta object for the container reference '{@link migration.MigrationDestination#getMigration <em>Migration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Migration</em>'.
	 * @see migration.MigrationDestination#getMigration()
	 * @see #getMigrationDestination()
	 * @generated
	 */
	EReference getMigrationDestination_Migration();

	/**
	 * Returns the meta object for enum '{@link migration.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Data Type</em>'.
	 * @see migration.DataType
	 * @generated
	 */
	EEnum getDataType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MigrationFactory getMigrationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link migration.impl.MigrationImpl <em>Migration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.MigrationImpl
		 * @see migration.impl.MigrationPackageImpl#getMigration()
		 * @generated
		 */
		EClass MIGRATION = eINSTANCE.getMigration();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MIGRATION__SOURCE = eINSTANCE.getMigration_Source();

		/**
		 * The meta object literal for the '<em><b>Destination</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MIGRATION__DESTINATION = eINSTANCE.getMigration_Destination();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MIGRATION__NAME = eINSTANCE.getMigration_Name();

		/**
		 * The meta object literal for the '<em><b>Source2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MIGRATION__SOURCE2 = eINSTANCE.getMigration_Source2();

		/**
		 * The meta object literal for the '{@link migration.impl.FromTableImpl <em>From Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.FromTableImpl
		 * @see migration.impl.MigrationPackageImpl#getFromTable()
		 * @generated
		 */
		EClass FROM_TABLE = eINSTANCE.getFromTable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FROM_TABLE__NAME = eINSTANCE.getFromTable_Name();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FROM_TABLE__FIELDS = eINSTANCE.getFromTable_Fields();

		/**
		 * The meta object literal for the '{@link migration.impl.ToTableImpl <em>To Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.ToTableImpl
		 * @see migration.impl.MigrationPackageImpl#getToTable()
		 * @generated
		 */
		EClass TO_TABLE = eINSTANCE.getToTable();

		/**
		 * The meta object literal for the '<em><b>Mapping</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TO_TABLE__MAPPING = eINSTANCE.getToTable_Mapping();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TO_TABLE__NAME = eINSTANCE.getToTable_Name();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TO_TABLE__FIELDS = eINSTANCE.getToTable_Fields();

		/**
		 * The meta object literal for the '<em><b>Migration</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TO_TABLE__MIGRATION = eINSTANCE.getToTable_Migration();

		/**
		 * The meta object literal for the '{@link migration.impl.MappingImpl <em>Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.MappingImpl
		 * @see migration.impl.MigrationPackageImpl#getMapping()
		 * @generated
		 */
		EClass MAPPING = eINSTANCE.getMapping();

		/**
		 * The meta object literal for the '<em><b>Table</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAPPING__TABLE = eINSTANCE.getMapping_Table();

		/**
		 * The meta object literal for the '{@link migration.impl.SingleMappingImpl <em>Single Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.SingleMappingImpl
		 * @see migration.impl.MigrationPackageImpl#getSingleMapping()
		 * @generated
		 */
		EClass SINGLE_MAPPING = eINSTANCE.getSingleMapping();

		/**
		 * The meta object literal for the '<em><b>From Table</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SINGLE_MAPPING__FROM_TABLE = eINSTANCE.getSingleMapping_FromTable();

		/**
		 * The meta object literal for the '{@link migration.impl.JoinMappingImpl <em>Join Mapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.JoinMappingImpl
		 * @see migration.impl.MigrationPackageImpl#getJoinMapping()
		 * @generated
		 */
		EClass JOIN_MAPPING = eINSTANCE.getJoinMapping();

		/**
		 * The meta object literal for the '<em><b>Join Field A</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JOIN_MAPPING__JOIN_FIELD_A = eINSTANCE.getJoinMapping_JoinFieldA();

		/**
		 * The meta object literal for the '<em><b>Join Field B</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JOIN_MAPPING__JOIN_FIELD_B = eINSTANCE.getJoinMapping_JoinFieldB();

		/**
		 * The meta object literal for the '{@link migration.impl.FromColumnImpl <em>From Column</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.FromColumnImpl
		 * @see migration.impl.MigrationPackageImpl#getFromColumn()
		 * @generated
		 */
		EClass FROM_COLUMN = eINSTANCE.getFromColumn();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FROM_COLUMN__NAME = eINSTANCE.getFromColumn_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FROM_COLUMN__TYPE = eINSTANCE.getFromColumn_Type();

		/**
		 * The meta object literal for the '<em><b>Table</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FROM_COLUMN__TABLE = eINSTANCE.getFromColumn_Table();

		/**
		 * The meta object literal for the '{@link migration.impl.ToColumnImpl <em>To Column</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.ToColumnImpl
		 * @see migration.impl.MigrationPackageImpl#getToColumn()
		 * @generated
		 */
		EClass TO_COLUMN = eINSTANCE.getToColumn();

		/**
		 * The meta object literal for the '<em><b>Column Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TO_COLUMN__COLUMN_NAME = eINSTANCE.getToColumn_ColumnName();

		/**
		 * The meta object literal for the '<em><b>Table</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TO_COLUMN__TABLE = eINSTANCE.getToColumn_Table();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TO_COLUMN__TYPE = eINSTANCE.getToColumn_Type();

		/**
		 * The meta object literal for the '{@link migration.impl.ReferenceColumnImpl <em>Reference Column</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.ReferenceColumnImpl
		 * @see migration.impl.MigrationPackageImpl#getReferenceColumn()
		 * @generated
		 */
		EClass REFERENCE_COLUMN = eINSTANCE.getReferenceColumn();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_COLUMN__FROM = eINSTANCE.getReferenceColumn_From();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation REFERENCE_COLUMN___GET_NAME = eINSTANCE.getReferenceColumn__GetName();

		/**
		 * The meta object literal for the '{@link migration.impl.MacroColumnImpl <em>Macro Column</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.MacroColumnImpl
		 * @see migration.impl.MigrationPackageImpl#getMacroColumn()
		 * @generated
		 */
		EClass MACRO_COLUMN = eINSTANCE.getMacroColumn();

		/**
		 * The meta object literal for the '<em><b>Params</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MACRO_COLUMN__PARAMS = eINSTANCE.getMacroColumn_Params();

		/**
		 * The meta object literal for the '<em><b>Macro Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MACRO_COLUMN__MACRO_NAME = eINSTANCE.getMacroColumn_MacroName();

		/**
		 * The meta object literal for the '<em><b>Macro Body</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MACRO_COLUMN__MACRO_BODY = eINSTANCE.getMacroColumn_MacroBody();

		/**
		 * The meta object literal for the '<em><b>Return Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MACRO_COLUMN__RETURN_TYPE = eINSTANCE.getMacroColumn_ReturnType();

		/**
		 * The meta object literal for the '<em><b>Columns</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MACRO_COLUMN__COLUMNS = eINSTANCE.getMacroColumn_Columns();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MACRO_COLUMN___GET_NAME = eINSTANCE.getMacroColumn__GetName();

		/**
		 * The meta object literal for the '{@link migration.impl.ConnectionURIImpl <em>Connection URI</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.ConnectionURIImpl
		 * @see migration.impl.MigrationPackageImpl#getConnectionURI()
		 * @generated
		 */
		EClass CONNECTION_URI = eINSTANCE.getConnectionURI();

		/**
		 * The meta object literal for the '<em><b>URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONNECTION_URI__URI = eINSTANCE.getConnectionURI_URI();

		/**
		 * The meta object literal for the '<em><b>Username</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONNECTION_URI__USERNAME = eINSTANCE.getConnectionURI_Username();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONNECTION_URI__PASSWORD = eINSTANCE.getConnectionURI_Password();

		/**
		 * The meta object literal for the '{@link migration.impl.MigrationSourceImpl <em>Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.MigrationSourceImpl
		 * @see migration.impl.MigrationPackageImpl#getMigrationSource()
		 * @generated
		 */
		EClass MIGRATION_SOURCE = eINSTANCE.getMigrationSource();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MIGRATION_SOURCE__FROM = eINSTANCE.getMigrationSource_From();

		/**
		 * The meta object literal for the '<em><b>Connection Uri</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MIGRATION_SOURCE__CONNECTION_URI = eINSTANCE.getMigrationSource_ConnectionUri();

		/**
		 * The meta object literal for the '<em><b>Migration</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MIGRATION_SOURCE__MIGRATION = eINSTANCE.getMigrationSource_Migration();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MIGRATION_SOURCE__NAME = eINSTANCE.getMigrationSource_Name();

		/**
		 * The meta object literal for the '{@link migration.impl.MigrationDestinationImpl <em>Destination</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.impl.MigrationDestinationImpl
		 * @see migration.impl.MigrationPackageImpl#getMigrationDestination()
		 * @generated
		 */
		EClass MIGRATION_DESTINATION = eINSTANCE.getMigrationDestination();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MIGRATION_DESTINATION__TO = eINSTANCE.getMigrationDestination_To();

		/**
		 * The meta object literal for the '<em><b>Connection Uri</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MIGRATION_DESTINATION__CONNECTION_URI = eINSTANCE.getMigrationDestination_ConnectionUri();

		/**
		 * The meta object literal for the '<em><b>Migration</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MIGRATION_DESTINATION__MIGRATION = eINSTANCE.getMigrationDestination_Migration();

		/**
		 * The meta object literal for the '{@link migration.DataType <em>Data Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see migration.DataType
		 * @see migration.impl.MigrationPackageImpl#getDataType()
		 * @generated
		 */
		EEnum DATA_TYPE = eINSTANCE.getDataType();

	}

} //MigrationPackage
