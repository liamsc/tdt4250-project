/**
 */
package migration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Join Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.JoinMapping#getJoinFieldA <em>Join Field A</em>}</li>
 *   <li>{@link migration.JoinMapping#getJoinFieldB <em>Join Field B</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getJoinMapping()
 * @model
 * @generated
 */
public interface JoinMapping extends Mapping {
	/**
	 * Returns the value of the '<em><b>Join Field A</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Join Field A</em>' reference.
	 * @see #setJoinFieldA(FromColumn)
	 * @see migration.MigrationPackage#getJoinMapping_JoinFieldA()
	 * @model
	 * @generated
	 */
	FromColumn getJoinFieldA();

	/**
	 * Sets the value of the '{@link migration.JoinMapping#getJoinFieldA <em>Join Field A</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Join Field A</em>' reference.
	 * @see #getJoinFieldA()
	 * @generated
	 */
	void setJoinFieldA(FromColumn value);

	/**
	 * Returns the value of the '<em><b>Join Field B</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Join Field B</em>' reference.
	 * @see #setJoinFieldB(FromColumn)
	 * @see migration.MigrationPackage#getJoinMapping_JoinFieldB()
	 * @model
	 * @generated
	 */
	FromColumn getJoinFieldB();

	/**
	 * Sets the value of the '{@link migration.JoinMapping#getJoinFieldB <em>Join Field B</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Join Field B</em>' reference.
	 * @see #getJoinFieldB()
	 * @generated
	 */
	void setJoinFieldB(FromColumn value);

} // JoinMapping
