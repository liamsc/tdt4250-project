/**
 */
package migration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.SingleMapping#getFromTable <em>From Table</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getSingleMapping()
 * @model
 * @generated
 */
public interface SingleMapping extends Mapping {
	/**
	 * Returns the value of the '<em><b>From Table</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Table</em>' reference.
	 * @see #setFromTable(FromTable)
	 * @see migration.MigrationPackage#getSingleMapping_FromTable()
	 * @model
	 * @generated
	 */
	FromTable getFromTable();

	/**
	 * Sets the value of the '{@link migration.SingleMapping#getFromTable <em>From Table</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Table</em>' reference.
	 * @see #getFromTable()
	 * @generated
	 */
	void setFromTable(FromTable value);

} // SingleMapping
