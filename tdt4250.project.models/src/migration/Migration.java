/**
 */
package migration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Migration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.Migration#getSource <em>Source</em>}</li>
 *   <li>{@link migration.Migration#getDestination <em>Destination</em>}</li>
 *   <li>{@link migration.Migration#getName <em>Name</em>}</li>
 *   <li>{@link migration.Migration#getSource2 <em>Source2</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getMigration()
 * @model
 * @generated
 */
public interface Migration extends EObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(MigrationSource)
	 * @see migration.MigrationPackage#getMigration_Source()
	 * @model required="true"
	 * @generated
	 */
	MigrationSource getSource();

	/**
	 * Sets the value of the '{@link migration.Migration#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(MigrationSource value);

	/**
	 * Returns the value of the '<em><b>Destination</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link migration.MigrationDestination#getMigration <em>Migration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination</em>' containment reference.
	 * @see #setDestination(MigrationDestination)
	 * @see migration.MigrationPackage#getMigration_Destination()
	 * @see migration.MigrationDestination#getMigration
	 * @model opposite="migration" containment="true"
	 * @generated
	 */
	MigrationDestination getDestination();

	/**
	 * Sets the value of the '{@link migration.Migration#getDestination <em>Destination</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination</em>' containment reference.
	 * @see #getDestination()
	 * @generated
	 */
	void setDestination(MigrationDestination value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see migration.MigrationPackage#getMigration_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link migration.Migration#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Source2</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link migration.MigrationSource#getMigration <em>Migration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source2</em>' containment reference.
	 * @see #setSource2(MigrationSource)
	 * @see migration.MigrationPackage#getMigration_Source2()
	 * @see migration.MigrationSource#getMigration
	 * @model opposite="migration" containment="true"
	 * @generated
	 */
	MigrationSource getSource2();

	/**
	 * Sets the value of the '{@link migration.Migration#getSource2 <em>Source2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source2</em>' containment reference.
	 * @see #getSource2()
	 * @generated
	 */
	void setSource2(MigrationSource value);

} // Migration
