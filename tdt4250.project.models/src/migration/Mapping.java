/**
 */
package migration;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.Mapping#getTable <em>Table</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getMapping()
 * @model abstract="true"
 * @generated
 */
public interface Mapping extends EObject {

	/**
	 * Returns the value of the '<em><b>Table</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link migration.ToTable#getMapping <em>Mapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Table</em>' container reference.
	 * @see #setTable(ToTable)
	 * @see migration.MigrationPackage#getMapping_Table()
	 * @see migration.ToTable#getMapping
	 * @model opposite="mapping" required="true" transient="false"
	 * @generated
	 */
	ToTable getTable();

	/**
	 * Sets the value of the '{@link migration.Mapping#getTable <em>Table</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Table</em>' container reference.
	 * @see #getTable()
	 * @generated
	 */
	void setTable(ToTable value);
} // Mapping
