/**
 */
package migration;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Column</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.ReferenceColumn#getFrom <em>From</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getReferenceColumn()
 * @model
 * @generated
 */
public interface ReferenceColumn extends ToColumn {
	/**
	 * Returns the value of the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference.
	 * @see #setFrom(FromColumn)
	 * @see migration.MigrationPackage#getReferenceColumn_From()
	 * @model required="true"
	 * @generated
	 */
	FromColumn getFrom();

	/**
	 * Sets the value of the '{@link migration.ReferenceColumn#getFrom <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(FromColumn value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getName();

} // ReferenceColumn
