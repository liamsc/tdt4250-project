/**
 */
package migration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Destination</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.MigrationDestination#getTo <em>To</em>}</li>
 *   <li>{@link migration.MigrationDestination#getConnectionUri <em>Connection Uri</em>}</li>
 *   <li>{@link migration.MigrationDestination#getMigration <em>Migration</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getMigrationDestination()
 * @model
 * @generated
 */
public interface MigrationDestination extends EObject {
	/**
	 * Returns the value of the '<em><b>To</b></em>' containment reference list.
	 * The list contents are of type {@link migration.ToTable}.
	 * It is bidirectional and its opposite is '{@link migration.ToTable#getMigration <em>Migration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' containment reference list.
	 * @see migration.MigrationPackage#getMigrationDestination_To()
	 * @see migration.ToTable#getMigration
	 * @model opposite="migration" containment="true"
	 * @generated
	 */
	EList<ToTable> getTo();

	/**
	 * Returns the value of the '<em><b>Connection Uri</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connection Uri</em>' containment reference.
	 * @see #setConnectionUri(ConnectionURI)
	 * @see migration.MigrationPackage#getMigrationDestination_ConnectionUri()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ConnectionURI getConnectionUri();

	/**
	 * Sets the value of the '{@link migration.MigrationDestination#getConnectionUri <em>Connection Uri</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Connection Uri</em>' containment reference.
	 * @see #getConnectionUri()
	 * @generated
	 */
	void setConnectionUri(ConnectionURI value);

	/**
	 * Returns the value of the '<em><b>Migration</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link migration.Migration#getDestination <em>Destination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Migration</em>' container reference.
	 * @see #setMigration(Migration)
	 * @see migration.MigrationPackage#getMigrationDestination_Migration()
	 * @see migration.Migration#getDestination
	 * @model opposite="destination" required="true" transient="false"
	 * @generated
	 */
	Migration getMigration();

	/**
	 * Sets the value of the '{@link migration.MigrationDestination#getMigration <em>Migration</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Migration</em>' container reference.
	 * @see #getMigration()
	 * @generated
	 */
	void setMigration(Migration value);

} // MigrationDestination
