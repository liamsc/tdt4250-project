/**
 */
package migration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>From Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.FromTable#getName <em>Name</em>}</li>
 *   <li>{@link migration.FromTable#getFields <em>Fields</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getFromTable()
 * @model
 * @generated
 */
public interface FromTable extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see migration.MigrationPackage#getFromTable_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link migration.FromTable#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
	 * The list contents are of type {@link migration.FromColumn}.
	 * It is bidirectional and its opposite is '{@link migration.FromColumn#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields</em>' containment reference list.
	 * @see migration.MigrationPackage#getFromTable_Fields()
	 * @see migration.FromColumn#getTable
	 * @model opposite="table" containment="true"
	 * @generated
	 */
	EList<FromColumn> getFields();

} // FromTable
