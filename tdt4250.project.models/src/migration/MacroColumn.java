/**
 */
package migration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.xbase.XExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Macro Column</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link migration.MacroColumn#getParams <em>Params</em>}</li>
 *   <li>{@link migration.MacroColumn#getMacroName <em>Macro Name</em>}</li>
 *   <li>{@link migration.MacroColumn#getMacroBody <em>Macro Body</em>}</li>
 *   <li>{@link migration.MacroColumn#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link migration.MacroColumn#getColumns <em>Columns</em>}</li>
 * </ul>
 *
 * @see migration.MigrationPackage#getMacroColumn()
 * @model
 * @generated
 */
public interface MacroColumn extends ToColumn {
	/**
	 * Returns the value of the '<em><b>Params</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.xtext.common.types.JvmFormalParameter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Params</em>' containment reference list.
	 * @see migration.MigrationPackage#getMacroColumn_Params()
	 * @model containment="true"
	 * @generated
	 */
	EList<JvmFormalParameter> getParams();

	/**
	 * Returns the value of the '<em><b>Macro Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Macro Name</em>' attribute.
	 * @see #setMacroName(String)
	 * @see migration.MigrationPackage#getMacroColumn_MacroName()
	 * @model
	 * @generated
	 */
	String getMacroName();

	/**
	 * Sets the value of the '{@link migration.MacroColumn#getMacroName <em>Macro Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Macro Name</em>' attribute.
	 * @see #getMacroName()
	 * @generated
	 */
	void setMacroName(String value);

	/**
	 * Returns the value of the '<em><b>Macro Body</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Macro Body</em>' containment reference.
	 * @see #setMacroBody(XExpression)
	 * @see migration.MigrationPackage#getMacroColumn_MacroBody()
	 * @model containment="true" required="true"
	 * @generated
	 */
	XExpression getMacroBody();

	/**
	 * Sets the value of the '{@link migration.MacroColumn#getMacroBody <em>Macro Body</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Macro Body</em>' containment reference.
	 * @see #getMacroBody()
	 * @generated
	 */
	void setMacroBody(XExpression value);

	/**
	 * Returns the value of the '<em><b>Return Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Type</em>' containment reference.
	 * @see #setReturnType(JvmTypeReference)
	 * @see migration.MigrationPackage#getMacroColumn_ReturnType()
	 * @model containment="true" required="true"
	 * @generated
	 */
	JvmTypeReference getReturnType();

	/**
	 * Sets the value of the '{@link migration.MacroColumn#getReturnType <em>Return Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Type</em>' containment reference.
	 * @see #getReturnType()
	 * @generated
	 */
	void setReturnType(JvmTypeReference value);

	/**
	 * Returns the value of the '<em><b>Columns</b></em>' reference list.
	 * The list contents are of type {@link migration.FromColumn}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Columns</em>' reference list.
	 * @see migration.MigrationPackage#getMacroColumn_Columns()
	 * @model required="true"
	 * @generated
	 */
	EList<FromColumn> getColumns();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getName();

} // MacroColumn
