/**
 * generated by Xtext 2.22.0
 */
package tdt4250.project.jvmmodel;

import com.google.inject.Inject;
import java.util.Arrays;
import migration.MacroColumn;
import migration.Migration;
import migration.MigrationDestination;
import migration.ToColumn;
import migration.ToTable;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmMember;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.xbase.jvmmodel.AbstractModelInferrer;
import org.eclipse.xtext.xbase.jvmmodel.IJvmDeclaredTypeAcceptor;
import org.eclipse.xtext.xbase.jvmmodel.JvmTypesBuilder;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

/**
 * <p>Infers a JVM model from the source model.</p>
 * 
 * <p>The JVM model should contain all elements that would appear in the Java code
 * which is generated from the source model. Other models link against the JVM model rather than the source model.</p>
 */
@SuppressWarnings("all")
public class ProjectXJvmModelInferrer extends AbstractModelInferrer {
  public static final String INFERRED_CLASS_NAME = "tdt4250.project.generated.MacroHelper";
  
  /**
   * convenience API to build and initialize JVM types and their members.
   */
  @Inject
  @Extension
  private JvmTypesBuilder _jvmTypesBuilder;
  
  /**
   * The dispatch method {@code infer} is called for each instance of the
   * given element's type that is contained in a resource.
   * 
   * @param element
   *            the model to create one or more
   *            {@link org.eclipse.xtext.common.types.JvmDeclaredType declared
   *            types} from.
   * @param acceptor
   *            each created
   *            {@link org.eclipse.xtext.common.types.JvmDeclaredType type}
   *            without a container should be passed to the acceptor in order
   *            get attached to the current resource. The acceptor's
   *            {@link IJvmDeclaredTypeAcceptor#accept(org.eclipse.xtext.common.types.JvmDeclaredType)
   *            accept(..)} method takes the constructed empty type for the
   *            pre-indexing phase. This one is further initialized in the
   *            indexing phase using the lambda you pass as the last argument.
   * @param isPreIndexingPhase
   *            whether the method is called in a pre-indexing phase, i.e.
   *            when the global index is not yet fully updated. You must not
   *            rely on linking using the index if isPreIndexingPhase is
   *            <code>true</code>.
   */
  protected void _infer(final Migration element, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    MigrationDestination _destination = element.getDestination();
    boolean _tripleEquals = (_destination == null);
    if (_tripleEquals) {
      System.out.println("Macro not generated");
      return;
    }
    boolean containMacro = false;
    EList<ToTable> _to = element.getDestination().getTo();
    for (final ToTable toTable : _to) {
      EList<ToColumn> _fields = toTable.getFields();
      for (final ToColumn toColumn : _fields) {
        if ((toColumn instanceof MacroColumn)) {
          containMacro = true;
        }
      }
    }
    if ((!containMacro)) {
      System.out.println("Macro not generated");
      return;
    } else {
      System.out.println("Macro generated");
    }
    final Procedure1<JvmGenericType> _function = (JvmGenericType it) -> {
      EList<ToTable> _to_1 = element.getDestination().getTo();
      for (final ToTable toTable_1 : _to_1) {
        EList<ToColumn> _fields_1 = toTable_1.getFields();
        for (final ToColumn toColumn_1 : _fields_1) {
          if ((toColumn_1 instanceof MacroColumn)) {
            final MacroColumn macroColumn = ((MacroColumn) toColumn_1);
            EList<JvmMember> _members = it.getMembers();
            final Procedure1<JvmOperation> _function_1 = (JvmOperation it_1) -> {
              EList<JvmFormalParameter> _params = macroColumn.getParams();
              for (final JvmFormalParameter param : _params) {
                EList<JvmFormalParameter> _parameters = it_1.getParameters();
                JvmFormalParameter _parameter = this._jvmTypesBuilder.toParameter(param, param.getName(), param.getParameterType());
                this._jvmTypesBuilder.<JvmFormalParameter>operator_add(_parameters, _parameter);
              }
              this._jvmTypesBuilder.setBody(it_1, macroColumn.getMacroBody());
            };
            JvmOperation _method = this._jvmTypesBuilder.toMethod(macroColumn.getMacroBody(), macroColumn.getMacroName(), macroColumn.getReturnType(), _function_1);
            this._jvmTypesBuilder.<JvmOperation>operator_add(_members, _method);
          }
        }
      }
    };
    acceptor.<JvmGenericType>accept(this._jvmTypesBuilder.toClass(element, ProjectXJvmModelInferrer.INFERRED_CLASS_NAME), _function);
  }
  
  public void infer(final EObject element, final IJvmDeclaredTypeAcceptor acceptor, final boolean isPreIndexingPhase) {
    if (element instanceof Migration) {
      _infer((Migration)element, acceptor, isPreIndexingPhase);
      return;
    } else if (element != null) {
      _infer(element, acceptor, isPreIndexingPhase);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(element, acceptor, isPreIndexingPhase).toString());
    }
  }
}
