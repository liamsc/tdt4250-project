/**
 * generated by Xtext 2.22.0
 */
package tdt4250.project.tests;

import com.google.inject.Inject;
import migration.Migration;
import migration.MigrationPackage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.testing.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import tdt4250.project.tests.ProjectXInjectorProvider;
import tdt4250.project.validation.ProjectXValidator;

@ExtendWith(InjectionExtension.class)
@InjectWith(ProjectXInjectorProvider.class)
@SuppressWarnings("all")
public class ProjectXParsingTest {
  @Inject
  private ParseHelper<Migration> parseHelper;
  
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
  
  @Test
  public void loadModel() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("name: mymodel");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("include: mymodel.myserversA");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("from: \"root\":\"example\" \"mysql://localhost:3306/table\" as myserversA containing:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("Firstname: text");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("Lastname: text");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("Birthdate: Date");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Occupations:");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("UserUUID: text");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("OccupationName: text");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Products:");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("Name: text");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("ProductId: int");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("to: \"tdt4250\":\"example\" \"postgresql://localhost:5432/tdt4250\" containing:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("users:");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("Join Users.UUID = Occupations.UserUUID");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("macro using Users.Firstname, Users.Lastname = concat(String firstname, String lastname) : String {");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.append("return String.format(\"%s %s\", firstname, lastname);");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("} text as \"name\"");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("Date Users.Birthdate as \"birthdate\"");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("text Occupations.OccupationName as \"occupation_name\"");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("products:");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("Map Products");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("text Products.Name as \"name\"");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("int Products.ProductId as \"product_id\" ");
      _builder.newLine();
      _builder.newLine();
      final Migration result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      this._validationTestHelper.assertNoErrors(result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testCheckDuplicateNamesFromTables() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("name: mymodel");
      _builder.newLine();
      _builder.append("include: mymodel.myserversA");
      _builder.newLine();
      _builder.newLine();
      _builder.append("from: \"tdt4250\":\"example\" \"mysql://localhost:3306/tdt4250\" as myserversA containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Firstname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Lastname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Birthdate: Date");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Occupations:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UserUUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("OccupationName: text");
      _builder.newLine();
      _builder.newLine();
      _builder.append("to: \"tdt4250\":\"example\" \"postgresql://localhost:5432/tdt4250\" containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Join Users.UUID = Occupations.UserUUID");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("macro using Users.Firstname, Users.Lastname = concat(String firstname, String lastname) : String {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("return String.format(\"%s %s\", firstname, lastname);");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("} text as \"name\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Date Users.Birthdate as \"birthdate\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("text Occupations.OccupationName as \"occupation_name\"");
      _builder.newLine();
      final Migration result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      this._validationTestHelper.assertError(result, MigrationPackage.Literals.MIGRATION_SOURCE, ProjectXValidator.DUPLICATE_NAME);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testCheckDuplicateNamesFromColumns() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("name: mymodel");
      _builder.newLine();
      _builder.append("include: mymodel.myserversA");
      _builder.newLine();
      _builder.newLine();
      _builder.append("from: \"tdt4250\":\"example\" \"mysql://localhost:3306/tdt4250\" as myserversA containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Firstname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Lastname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Birthdate: Date");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Occupations:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UserUUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("OccupationName: text");
      _builder.newLine();
      _builder.newLine();
      _builder.append("to: \"tdt4250\":\"example\" \"postgresql://localhost:5432/tdt4250\" containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Join Users.UUID = Occupations.UserUUID");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("macro using Users.Firstname, Users.Lastname = concat(String firstname, String lastname) : String {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("return String.format(\"%s %s\", firstname, lastname);");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("} text as \"name\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Date Users.Birthdate as \"birthdate\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("text Occupations.OccupationName as \"occupation_name\"");
      _builder.newLine();
      final Migration result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      this._validationTestHelper.assertError(result, MigrationPackage.Literals.FROM_TABLE, ProjectXValidator.DUPLICATE_NAME);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testCheckDuplicateNamesToTable() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("name: mymodel");
      _builder.newLine();
      _builder.append("include: mymodel.myserversA");
      _builder.newLine();
      _builder.newLine();
      _builder.append("from: \"tdt4250\":\"example\" \"mysql://localhost:3306/tdt4250\" as myserversA containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Firstname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Lastname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Birthdate: Date");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Occupations:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UserUUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("OccupationName: text");
      _builder.newLine();
      _builder.newLine();
      _builder.append("to: \"tdt4250\":\"example\" \"postgresql://localhost:5432/tdt4250\" containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Join Users.UUID = Occupations.UserUUID");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("macro using Users.Firstname, Users.Lastname = concat(String firstname, String lastname) : String {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("return String.format(\"%s %s\", firstname, lastname);");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("} text as \"name\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Date Users.Birthdate as \"birthdate\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("text Occupations.OccupationName as \"occupation_name\"");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Map Users");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      final Migration result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      this._validationTestHelper.assertError(result, MigrationPackage.Literals.MIGRATION_DESTINATION, ProjectXValidator.DUPLICATE_NAME);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testCheckDuplicateNamesToColumns() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("name: mymodel");
      _builder.newLine();
      _builder.append("include: mymodel.myserversA");
      _builder.newLine();
      _builder.newLine();
      _builder.append("from: \"tdt4250\":\"example\" \"mysql://localhost:3306/tdt4250\" as myserversA containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Firstname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Lastname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Birthdate: Date");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Occupations:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UserUUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("OccupationName: text");
      _builder.newLine();
      _builder.newLine();
      _builder.append("to: \"tdt4250\":\"example\" \"postgresql://localhost:5432/tdt4250\" containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Join Users.UUID = Occupations.UserUUID");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("macro using Users.Firstname, Users.Lastname = concat(String firstname, String lastname) : String {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("return String.format(\"%s %s\", firstname, lastname);");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("} text as \"name\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Date Users.Birthdate as \"birthdate\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("text Occupations.OccupationName as \"occupation_name\"");
      _builder.newLine();
      final Migration result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      this._validationTestHelper.assertError(result, MigrationPackage.Literals.TO_TABLE, ProjectXValidator.DUPLICATE_NAME);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testCheckMacroColumnArgumentsTooFew() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("name: mymodel");
      _builder.newLine();
      _builder.append("include: mymodel.myserversA");
      _builder.newLine();
      _builder.newLine();
      _builder.append("from: \"tdt4250\":\"example\" \"mysql://localhost:3306/tdt4250\" as myserversA containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Firstname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Lastname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Birthdate: Date");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Occupations:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UserUUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("OccupationName: text");
      _builder.newLine();
      _builder.newLine();
      _builder.append("to: \"tdt4250\":\"example\" \"postgresql://localhost:5432/tdt4250\" containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Join Users.UUID = Occupations.UserUUID");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("macro using Users.Firstname = concat(String firstname, String lastname) : String {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("return String.format(\"%s %s\", firstname, lastname);");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("} text as \"name\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Date Users.Birthdate as \"birthdate\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("text Occupations.OccupationName as \"occupation_name\"");
      _builder.newLine();
      final Migration result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      this._validationTestHelper.assertError(result, MigrationPackage.Literals.MACRO_COLUMN, ProjectXValidator.INVALID_MACRO_ARGUMENTS_SIZE);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testCheckMacroColumnArgumentsTooMany() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("name: mymodel");
      _builder.newLine();
      _builder.append("include: mymodel.myserversA");
      _builder.newLine();
      _builder.newLine();
      _builder.append("from: \"tdt4250\":\"example\" \"mysql://localhost:3306/tdt4250\" as myserversA containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Firstname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Lastname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Birthdate: Date");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Occupations:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UserUUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("OccupationName: text");
      _builder.newLine();
      _builder.newLine();
      _builder.append("to: \"tdt4250\":\"example\" \"postgresql://localhost:5432/tdt4250\" containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Join Users.UUID = Occupations.UserUUID");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("macro using Users.Firstname, Users.Lastname, Users.UUID = concat(String firstname, String lastname) : String {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("return String.format(\"%s %s\", firstname, lastname);");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("} text as \"name\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Date Users.Birthdate as \"birthdate\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("text Occupations.OccupationName as \"occupation_name\"");
      _builder.newLine();
      final Migration result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      this._validationTestHelper.assertError(result, MigrationPackage.Literals.MACRO_COLUMN, ProjectXValidator.INVALID_MACRO_ARGUMENTS_SIZE);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testCheckConnectionURI() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("name: mymodel");
      _builder.newLine();
      _builder.append("include: mymodel.myserversA");
      _builder.newLine();
      _builder.newLine();
      _builder.append("from: \"tdt4250\":\"example\" \"mysql://localhost:3306/tdt4250\" as myserversA containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Firstname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Lastname: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Birthdate: Date");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("Occupations:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("UserUUID: text");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("OccupationName: text");
      _builder.newLine();
      _builder.newLine();
      _builder.append("to: \"tdt4250\":\"example\" \"https://localhost:5432/tdt4250\" containing:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("users:");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Join Users.UUID = Occupations.UserUUID");
      _builder.newLine();
      _builder.append("\t\t");
      _builder.append("Columns:");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("uuid Users.UUID as \"uuid\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("macro using Users.Firstname, Users.Lastname = concat(String firstname, String lastname) : String {");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("return String.format(\"%s %s\", firstname, lastname);");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("} text as \"name\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("Date Users.Birthdate as \"birthdate\"");
      _builder.newLine();
      _builder.append("\t\t\t");
      _builder.append("text Occupations.OccupationName as \"occupation_name\"");
      _builder.newLine();
      final Migration result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      this._validationTestHelper.assertError(result, MigrationPackage.Literals.CONNECTION_URI, ProjectXValidator.INVALID_DATABASE_URI);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void loadFromMultipleFiles() {
    try {
      final XtextResourceSet rs = new XtextResourceSet();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("name: mymodelA");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("include: mymodelA.myserversA");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.append("from: \"root\":\"example\" \"mysql://localhost:3306/table\" as myserversA containing:");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("Users:");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.append("UUID: text");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.append("Firstname: text");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.append("Lastname: text");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.append("Birthdate: Date");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("Occupations:");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.append("UserUUID: text");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.append("OccupationName: text");
      _builder.newLine();
      _builder.append("\t\t\t\t\t");
      _builder.append("Products:");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.append("Name: text");
      _builder.newLine();
      _builder.append("\t\t\t\t\t\t");
      _builder.append("ProductId: int");
      _builder.newLine();
      _builder.append("\t\t\t\t");
      _builder.newLine();
      final Migration first = this.parseHelper.parse(_builder, rs);
      Assertions.assertNotNull(first);
      EList<Resource.Diagnostic> errors = first.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: ");
      String _join = IterableExtensions.join(errors, ", ");
      _builder_1.append(_join);
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
      StringConcatenation _builder_2 = new StringConcatenation();
      _builder_2.append("name: mymodel");
      _builder_2.newLine();
      _builder_2.append("\t\t");
      _builder_2.append("include: mymodelA.myserversA");
      _builder_2.newLine();
      _builder_2.append("\t\t");
      _builder_2.newLine();
      _builder_2.append("\t\t");
      _builder_2.append("to: \"tdt4250\":\"example\" \"postgresql://localhost:5432/tdt4250\" containing:");
      _builder_2.newLine();
      _builder_2.append("\t\t\t");
      _builder_2.append("users:");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t");
      _builder_2.append("Join Users.UUID = Occupations.UserUUID");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t");
      _builder_2.append("Columns:");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t\t");
      _builder_2.append("uuid Users.UUID as \"uuid\"");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t\t");
      _builder_2.append("macro using Users.Firstname, Users.Lastname = concat(String firstname, String lastname) : String {");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t\t\t");
      _builder_2.append("return String.format(\"%s %s\", firstname, lastname);");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t\t");
      _builder_2.append("} text as \"name\"");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t\t");
      _builder_2.append("Date Users.Birthdate as \"birthdate\"");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t\t");
      _builder_2.append("text Occupations.OccupationName as \"occupation_name\"");
      _builder_2.newLine();
      _builder_2.append("\t\t");
      _builder_2.newLine();
      _builder_2.append("\t\t\t");
      _builder_2.append("products:");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t");
      _builder_2.append("Map Products");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t");
      _builder_2.append("Columns:");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t\t");
      _builder_2.append("text Products.Name as \"name\"");
      _builder_2.newLine();
      _builder_2.append("\t\t\t\t\t");
      _builder_2.append("int Products.ProductId as \"product_id\" ");
      _builder_2.newLine();
      final Migration second = this.parseHelper.parse(_builder_2, rs);
      Assertions.assertNotNull(second);
      errors = second.eResource().getErrors();
      boolean _isEmpty_1 = errors.isEmpty();
      StringConcatenation _builder_3 = new StringConcatenation();
      _builder_3.append("Unexpected errors: ");
      String _join_1 = IterableExtensions.join(errors, ", ");
      _builder_3.append(_join_1);
      Assertions.assertTrue(_isEmpty_1, _builder_3.toString());
      this._validationTestHelper.assertNoErrors(first);
      this._validationTestHelper.assertNoErrors(second);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
