CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "users" (
	"uuid" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
	"name" text,
	"birthdate" Date,
	"occupation_name" text
);

CREATE TABLE "products" (
	"product_id" INT PRIMARY KEY NOT NULL,
	"name" text
);