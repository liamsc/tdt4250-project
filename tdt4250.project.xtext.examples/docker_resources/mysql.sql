CREATE TABLE `Users` (
`UUID` varchar(36) PRIMARY KEY,
`Firstname` text,
`Lastname` text,
`Birthdate` Date
);

CREATE TABLE `Occupations` (
UserUUID varchar(36) PRIMARY KEY,
OccupationName text
);

CREATE TABLE `Products` (
ProductId varchar(10) PRIMARY KEY,
name text
);

INSERT INTO `Users` (`UUID`, `Firstname`, `Lastname`, `Birthdate`) VALUES ('cbad7a49-e2fd-4125-a078-61a0311e8bc8', 'Bob', 'Ross', NOW()), ('d9d20d99-6acc-4cfd-a93b-9a6e63730291', 'Charles', 'Roger', NOW());

INSERT INTO `Occupations` (`UserUUID`, `OccupationName`) VALUES ('cbad7a49-e2fd-4125-a078-61a0311e8bc8', 'Painter'), ('d9d20d99-6acc-4cfd-a93b-9a6e63730291', 'Plumber');

INSERT INTO `Products` (`ProductId`, `Name`) VALUES (1, 'John rogers cool accessory'), (2, 'Paintbrush');