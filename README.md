# TDT4250 Project

This is the main repository for the TDT4250 project of:

 * Andreas Jensen Jonassen
 * Liam Svanåsbakken Crouch

## Motivation

When moving from one information system to another, one of the hurdles involved is the migration of data from the old schema to the new. Many issues may arise in this process, such as the database servers or the schema being different. After all, two teams of software developers may have very different views on how things should be. Different requirements will also affect this.

We want to solve this by writing a generalized database migration language that reduces the complexity of transferring data between different SQL databases, to a simple DSL which allows you to declare database structures, and how data is supposed to be copied between them.

## Features
* DSL Backed by a custom Ecore Model
* Custom Scoping for nice syntax and correct autocompletion
* Xbase to allow for native Java code in macros
* Custom Generator that generates code through xbase and acceleo
  * JvmModelInferrer for xbase transformation
  * Acceleo for model transformation
* Custom Validator for constraints in the DSL
  * For example, enforing a ban on duplicate names and illegal database urls
* Unit tests of parsing and xtext validators

## Walkthrough

### Installation Guide

 * Clone the repo to a folder on your computer: `git clone https://gitlab.stud.idi.ntnu.no/liamsc/tdt4250-project.git`

#### Setting up the eclipse workspace

The repository acts as an eclipse workspace. To get started, boot up Eclipse, and select the cloned repository folder as your workspace. This guide assumes you have all plugins recommended by the course already installed. 

Once Eclipse has booted, you can import the projects into your workspace. Select **Import projects**, either from the empty `Package Explorer`, or from the `File` drop down menu at the top of the Eclipse window. Select **Existing Projects into Workspace** from the list of import wizards, and press **Next**.

![The import wizard](tutorial/import.png)

In the next screen, select the cloned repository as root directory. After doing this, all supplied projects should appear in the `Projects`-list. Make sure all are selected, and press **Finish**

![All projects needed for import](tutorial/allprojects.png)

You can now set up the Eclipse run-configuration. Press the down-arrow next to the play button, and press **Run configurations**. In the list to the left in the window that appears, select `Eclipse Application` and press the **New configuration** button located right above the list. You should be able to leave most things as they are. However, in the `Plug-ins` tab, it is recommended that you change `Launch with` to be with **plug-ins selected below only**, followed by deselecting:

 * `tdt4250.project.models.tests`
 * `tdt4250.project.xtext.examples`
 * `tdt4250.project.xtext.tests`
 * `tdt4250.project.xtext.ui.tests`

Other launch configurations may work, but mileage may vary!

![Our recommended launch configuration](tutorial/eclipse_configurations.png)

#### Generating some code

After launching your new Eclipse launch configuration, you should be met with an Eclipse instance with no projects in the workspace. Note that this workspace is separate from the folder you cloned the project to, this detail is important to remember for later.

You now have to set up a project you can use to build some custom code. We have provided an example in the `tdt4250.project.xtext.examples` project. However, we have not been able to resolve file locations outside the workspace root, for files that are contained within the workspace. Therefore, simply importing the `tdt4250.project.xtext.examples` project from its actual location on disk will not work well with generation - the generated files will be generated relative to the workspace root, independent of where the project is actually located. To copy over our example project, you have two options:

 * Copy the `tdt4250.project.xtext.examples` project to your new workspace. You will need to follow all the steps of [Building the java project](#building-the-java-project) anyways.
 * **(Recommended)** Create a new project

We will walk you through the creation of a new project, and copying over the required files.

Create a new project. The name can be whatever you want, for example `CodeGenerationTest`. For now, we can go with something as simple as a Java project. You can say no to creating a module info file.

![Creating a Java project](tutorial/create_java.png)

To finish off, copy over the following files from the `tdt4250.project.xtext.examples` project:

 * `simpleMigration_definitions.projectx` - Demonstrates the ability to read FromTable-definitions from another file
 * `simpleMigration.projectx` - Demonstrates code generation of a simple migration.

We recommend copying the files to the root of the project. After refreshing the project, eclipse should prompt you convert the project to an xtext project. Say yes to this prompt.

If everything goes well, and the files parse, Eclipse should automatically generate code for the `.projectx`-files you copied in. Refresh the project again to find them - they should be in a new folder called `src-gen-simpleMigration`. If not, try making an insignificant change to `simpleMigration.projectx` and saving. 

At this point, especially after re-generating the model, your `.projectx` file may complain that _The type MacroHelper is already defined in MacroHelper.java_. This happens because of how we generate macros. We did not have time to fix the issue, but as a workaround, you can delete the package inside `src-gen-simpleMigration` and save `simpleMigration.projectx` again to force a clean re-generation, should this be needed.

#### Building the java project

You have reached this step when you have successfully generated Java-code. From here, the first thing you have to do is enable the `src-gen-simpleMigration` folder as a source folder. You can do this by right-clicking your generation project, and chosing **Properties** from the context menu. Navigate to **Java Build Path** in the list on the left, and make sure you are in the **Source** tab. Select **Add Folder...**, and add `src-gen-simpleMigration`. The java build path should look like this when you are done:

![The Java build path settings, with the correct build path configuration](tutorial/buildpath.png)

Next, you have to add database drivers. You can use a build system like Maven, or you can import the JAR files yourself. We have provided the `.jar`-file of both a MySQL and PostgreSQL driver. These drivers reside in the root of `tdt4250.project.xtext.examples` - `mysql-connector-java-5.1.49.jar` and `postgresql-42.2.18.jar`. Assuming you are still in the **Java Build Path** properties for the project, navigate to the **Libraries** tab. Select either **Modulepath** or **Classpath** (Both work), and click the **Add External JARs...** button on the right side of the window. Navigate to the location of the provided database drivers, and select both of them. When you are done, things should look a little like this:

![The Libraries tab with the sql drivers installed](tutorial/libraries.png)


#### Starting SQL servers

As this project generates a database migration tool, you need some database servers set up with the correct schemas in order to test that the tool works. To aid with this, we have provided a `docker-compose.yml` file, allowing you to start Docker containers that can be used for testing. These containers are pre-installed with the neccesary schemas and accounts, and should work out of the box. The requirements for using this, is **Docker** and **docker-compose**. See [Install Docker Compose](https://docs.docker.com/compose/install/) for more information.

The docker-compose file exists in the `tdt4250.project.xtext.examples` project, inside the subfolder `docker_resources`. The following is a list of containers that are launched, and why:

 * `pgsql` - PostgreSQL 11 server. Username: `tdt4250`, password: `example`. A database named `tdt4250` is created. Automatically set up using the `postgre.sql` file.
 * `mysql` - MySQL 5.7 server. Username: `tdt4250`, password: `example`. A database named `tdt4250` is created. Automatically set up using the `mysql.sql` file.
 * `adminer` - A lightweight web-based administration tool that can be used to inspect the contents of the database servers. It is accessable at `localhost:3739`. To connect to the database servers, use their docker-compose name(`pgsql`/`mysql`) as hostname.

Read the `docker-compose.yml` file for more information.

**NOTE**: All the containers above run with exposed ports, making the computer potentially vulnerable to remote attacks if not maintained. The ports are also accessable to other machines on the network. Exercise good dilligence when using docker services - shut down the containers when you are done with them.

Open a terminal, navigate to the folder containing `docker-compose.yml`, and run the command `docker-compose up` to launch the containers. The initialization process(including running our initialization scripts) may take some time, please wait for the servers to stop producing output for several seconds before running anything against them. 

To stop the containers when you are done, press Ctrl+C. To delete the containers, run `docker-compose down`. If you ran the containers detatched(`docker-compose up -d`), you have to run ` docker-compose down` twice - once to stop the containers, and once to delete them. The state of the SQL servers are not volume-mounted, so deleting the containers and starting them again is enough to reset.

#### Running the generated code

With the database servers running, and code generated, you should be ready to run some generated migration tool code! The most foolproof way of doing this is to navigate to the `MigrationMain.java` generated file, **Right click**ing on it, choosing **Run as** from the context menu, followed by **Java Application**. If you have done everything in this guide correctly, your output should look like this:

```
Xtext-based database migrator
Sun Dec 13 16:15:02 CET 2020 WARN: Establishing SSL connection without server's identity verification is not recommended. According to MySQL 5.5.45+, 5.6.26+ and 5.7.6+ requirements SSL connection must be established by default if explicit option isn't set. For compliance with existing applications not using SSL the verifyServerCertificate property is set to 'false'. You need either to explicitly disable SSL by setting useSSL=false, or set useSSL=true and provide truststore for server certificate verification.
Executing migration for table users
Data fetching query: SELECT  Users.UUID,  Users.Firstname as Firstname_macro,  Users.Lastname as Lastname_macro,  Users.Birthdate,  Occupations.OccupationName FROM Users  INNER JOIN Occupations ON Users.UUID = Occupations.UserUUID;
Row
tdt4250.project.acceleo.generated.usersColumn@368247b9
Row
tdt4250.project.acceleo.generated.usersColumn@1a6d8329
Inserting with prepared statement: INSERT INTO users (	"uuid","name","birthdate","occupation_name" ) VALUES ( 'cbad7a49-e2fd-4125-a078-61a0311e8bc8', ?, ?, ?);
Inserting with prepared statement: INSERT INTO users (	"uuid","name","birthdate","occupation_name" ) VALUES ( 'd9d20d99-6acc-4cfd-a93b-9a6e63730291', ?, ?, ?);
Executing migration for table products
Data fetching query: SELECT  Products.Name,  Products.ProductId FROM Products ;
Row
tdt4250.project.acceleo.generated.productsColumn@7674f035
Row
tdt4250.project.acceleo.generated.productsColumn@69e153c5
Inserting with prepared statement: INSERT INTO products (	"name","product_id" ) VALUES ( ?, ?);
Inserting with prepared statement: INSERT INTO products (	"name","product_id" ) VALUES ( ?, ?);
Done migrating!
```

### Features walkthrough
#### Custom validator
To view the custom validator at work you can simply add a duplicate table and/or column in either `from (Migration Source)` or `to (Migration Destination)`.
1. Go into `simpleMigration.projectx`
2. Duplicate the `Users` table in the `from (Migration Source)`, this should give an error
3. Duplicate the `UUID` column in the `Users` table, this should give an error
4. Duplicate the `users` table in the `to (Migration Destination)`, this should give an error
5. Duplicate the `uuid` column in the `users` table, this should give an error

You can also try to edit either of the database urls to use the `https` protocol instead of either `mysql` or `postgresql` protocols.
1. Go into `simpleMigration.projectx`
2. Change "postgresql://localhost:5432/tdt4250" to "https://localhost:5432/tdt4250", this should give an  error

We also make sure you cannot have more function arguments than FromTables in a macro column. To test this:

1. Go to `simpleMigration.projectx`
2. Delete the mention of `Users.Lastname` in the macro column of `users` - before the `=` character.
3. Observe the error.

#### Custom Scoping for nice syntax and correct autocompletion
This feature can be seen all throughout the syntax. Normally you would have to write out the full name of f.ex. a column like `mymodel.myserversA.Users.UUID`, but thanks to the scoping you only need to write `Users.UUID`.

The scoping also allows you to only get valid autocompletions. F.ex. in a "to table" with a "JoinMapping", you are only given autocompletion for "from tables" and "from columns" from the two tables in the "JoinMapping".
1. Go into `SimpleMigration.projectx`
2. Under the "to table" `users` try to remove first part of the "JoinMapping" i.e. `Users.UUID` and trigger your autocompletion (possibly ctrl+space) to get suggestion with proper scoping.
3. Under the "to table" `users` and the column `uuid` try to remove `Users.UUID` and trigger your autocompletion to see you only get suggestion from the tables `Users` and `Occupations` from the "JoinMapping".

#### Auto Generated Code with custom Generators
To see the generated code from acceleo and xbase (which results in a fully working java application that allows for migration from source to destination) and how to run it; see the [Installation guide - Generating some code](#generating-some-code).

#### Tests

We wrote a few tests to ensure that parsing, as well as our xtext validators, worked as intended. You can run them by right-clicking the `tdt4250.project.xtext.tests` project, chosing **Run as** from the context menu, followed by **JUnit test**. The tests should all be green. 

## DSL Syntax
### Forewords
It should be noted that the syntax of having to name your migration declaration and include the "Migration Source" in every file, even the declaration file (where you will have to include the declaration you just wrote) is an unfortunate evil we have to accept as we didn't have time to to find an more elegant solution where you only needed to include a "Migration Source" in the main migration file.

### Required fields in every file
To get started off you will have to name your model and import a "from table" that is either declared in the same file or in a separate file.
```
name: myCoolMigration
include: myCoolMigration.myMySQLServer
```

### Migration Source
To Declare a "Migration Source" you simply write `from:`, the username and password divided by a semicolon, the database url, the name of the "Migration source", and then you end it with `containing:` to signify that from here you will declare the tables and columns (with datatype) as seen below.
```
from: "root":"example" "mysql://localhost:3306/table" as myserverA containing:
	Users:
		UUID: text
		Firstname: text
		Lastname: text
		Birthdate: Date
	Occupations:
		UserUUID: text
		OccupationName: text
	Products:
		Name: text
		ProductId: int
```
It should be notated that if you choose to declare it in another file, f.ex. 'declaration.projectx' then that file will also be required to have a name and an include, as seen below:
```
name: myCoolDeclaration
include: myCoolDeclaration.myserverA

from: "root":"example" "mysql://localhost:3306/table" as myserverA containing:
	Users:
		UUID: text
```

### Migration Destination
To declare a "Migration Destination" is very similar to a "Migration Source", you start of similarly, but with 'to:' instead of 'from:' as seen below
```
to: "root":"example" "postgresql://localhost:5432/table" containing:
  table:
    [...]
  another_table:
    [...]
```
What differs however is the mapping between tables and columns - the transformation of data is declared in the micration destination declaration. First off, you have two alternatives for the mapping, either a "SingleMapping" or a "JoinMapping". The "SingleMapping" is simply that you select a single "From table" from the "Migration Source", whereas the "JoinMapping" declares a SQL Join between two tables, which can then be used to transform the contents, just like with a "SingleMapping".

SingleMapping is declared with 'Map' and a "from table" as seen below:
```
to: "root":"example" "postgresql://localhost:5432/table" containing:
  products:
    Map Products
    Columns:
      [...]
```

JoinMapping is declared with 'Join' and the two "from tables" and their column you want to join on:
```
to: "root":"example" "postgresql://localhost:5432/table" containing:
  users:
    Join Users.UUID = Occupations.UserUUID
    Columns:
      [...]
```

Note that the tables being referred to in the join are "FromTable"s.

To declare the columns in your new "to table"s you can either just choose a column from your mapping and give it a name as seen below:
```
to: "root":"example" "postgresql://localhost:5432/table" containing:
  users:
    Join Users.UUID = Occupations.UserUUID
    Columns:
      Users.UUID as "uuid"
```

...or you can declare a custom macro that takes in 1-N columns from your mapping and creates a single value that can be inserted into your column as seen below:

```
to: "root":"example" "postgresql://localhost:5432/table" containing:
  users:
    Join Users.UUID = Occupations.UserUUID
    Columns:
      macro using Users.Firstname, Users.Lastname = concat(String firstname, String lastname) : String {
        return String.format("%s %s", firstname, lastname);
	  } text as "name"
```

A full single file example could for example be seen below:
```
name: mymodel
include: mymodel.myserversA

from: "tdt4250":"example" "mysql://localhost:3306/tdt4250" as myserversA containing:
	Users:
		UUID: text
		Firstname: text
		Lastname: text
		Birthdate: Date
	Occupations:
		UserUUID: text
		OccupationName: text
	Products:
		Name: text
		ProductId: int


to: "tdt4250":"example" "postgresql://localhost:5432/tdt4250" containing:
	users:
		Join Users.UUID = Occupations.UserUUID
		Columns:
			uuid Users.UUID as "uuid"
			macro using Users.Firstname, Users.Lastname = concat(String firstname, String lastname) : String {
				return String.format("%s %s", firstname, lastname);
			} text as "name"
			Date Users.Birthdate as "birthdate"
			text Occupations.OccupationName as "occupation_name"

	products:
		Map Products
		Columns:
			text Products.Name as "name"
			int Products.ProductId as "product_id" 
```

## Afterthoughts

After having made a proof of concept for our solution, our conclusion is that we are happy with having learned acceleo, xtext, ecore, and the surrounding technologies. Our first impression was that the technologies we were learning were very advanced and heavy for our use-case. However, we are happy to have realied that we were wrong.

We were especially happy with how much of the process was automatic. Due to the use of ecore and xtext, we had very fast development iterations at the end. After refactoring the ecore model, it could take as little as 30 seconds, and only a few button presses, from we first started building ecore code, to testing the new language in Eclipse. This turned out to be very valuable, as it allowed us to quickly fix bugs and make last-minute improvements in the language.

## The Road Ahead

Of course, we were not able to create a feature complete perfect tool in the span of one course over one semester. There are therefore multiple things we would improve, had it been a perfect world:

 * Fix the unfortunate evil of having to name and include "Migration Source" in every file.
 * Support for both MySQL and PostgreSQL for both the Source and Destination.
   - At the moment we only support migration from MySQL to PostgreSQL
 * More data types
 * Improve error messages
   - Some errors, especially caused by writing with the wrong syntax, are very hard to understand.
 * Implement the formatter API to allow for auto-formatting of the `.projectx` files.
 * Support for multiple `from`-sources, such that data could be merged from multiple sources into one.
   - This would be especially useful in situations where you are migrating from a situation in which your systems are scattered, to a single information system that covers everything.
 * Generate code that scans the databases making sure the declarations are correct, before migrating anything.
 * Command-line utility for generating code from `.projectx` files
