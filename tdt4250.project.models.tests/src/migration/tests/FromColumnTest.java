/**
 */
package migration.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import migration.FromColumn;
import migration.MigrationFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>From Column</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class FromColumnTest extends TestCase {

	/**
	 * The fixture for this From Column test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FromColumn fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FromColumnTest.class);
	}

	/**
	 * Constructs a new From Column test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FromColumnTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this From Column test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(FromColumn fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this From Column test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FromColumn getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(MigrationFactory.eINSTANCE.createFromColumn());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //FromColumnTest
