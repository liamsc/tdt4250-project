/**
 */
package migration.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import migration.ConnectionURI;
import migration.MigrationFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Connection URI</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConnectionURITest extends TestCase {

	/**
	 * The fixture for this Connection URI test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionURI fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ConnectionURITest.class);
	}

	/**
	 * Constructs a new Connection URI test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConnectionURITest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Connection URI test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ConnectionURI fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Connection URI test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionURI getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(MigrationFactory.eINSTANCE.createConnectionURI());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ConnectionURITest
