/**
 */
package migration.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>MigrationGenerator</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class MigrationGeneratorAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new MigrationGeneratorAllTests("MigrationGenerator Tests");
		suite.addTest(MigrationTests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MigrationGeneratorAllTests(String name) {
		super(name);
	}

} //MigrationGeneratorAllTests
