/**
 */
package migration.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import migration.Migration;
import migration.MigrationFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Migration</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MigrationTest extends TestCase {

	/**
	 * The fixture for this Migration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Migration fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MigrationTest.class);
	}

	/**
	 * Constructs a new Migration test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MigrationTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Migration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Migration fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Migration test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Migration getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(MigrationFactory.eINSTANCE.createMigration());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MigrationTest
