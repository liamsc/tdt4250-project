/**
 */
package migration.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import migration.MigrationDestination;
import migration.MigrationFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Destination</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MigrationDestinationTest extends TestCase {

	/**
	 * The fixture for this Destination test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MigrationDestination fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MigrationDestinationTest.class);
	}

	/**
	 * Constructs a new Destination test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MigrationDestinationTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Destination test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(MigrationDestination fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Destination test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MigrationDestination getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(MigrationFactory.eINSTANCE.createMigrationDestination());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MigrationDestinationTest
