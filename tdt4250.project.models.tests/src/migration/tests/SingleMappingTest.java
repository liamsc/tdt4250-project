/**
 */
package migration.tests;

import junit.textui.TestRunner;

import migration.MigrationFactory;
import migration.SingleMapping;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Single Mapping</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SingleMappingTest extends MappingTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SingleMappingTest.class);
	}

	/**
	 * Constructs a new Single Mapping test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleMappingTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Single Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SingleMapping getFixture() {
		return (SingleMapping)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(MigrationFactory.eINSTANCE.createSingleMapping());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SingleMappingTest
