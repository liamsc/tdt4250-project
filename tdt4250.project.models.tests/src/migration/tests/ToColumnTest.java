/**
 */
package migration.tests;

import junit.framework.TestCase;

import migration.ToColumn;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>To Column</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ToColumnTest extends TestCase {

	/**
	 * The fixture for this To Column test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ToColumn fixture = null;

	/**
	 * Constructs a new To Column test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ToColumnTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this To Column test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ToColumn fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this To Column test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ToColumn getFixture() {
		return fixture;
	}

} //ToColumnTest
