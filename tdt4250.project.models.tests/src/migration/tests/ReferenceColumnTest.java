/**
 */
package migration.tests;

import junit.textui.TestRunner;

import migration.MigrationFactory;
import migration.ReferenceColumn;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Reference Column</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link migration.ReferenceColumn#getName() <em>Get Name</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ReferenceColumnTest extends ToColumnTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ReferenceColumnTest.class);
	}

	/**
	 * Constructs a new Reference Column test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceColumnTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Reference Column test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ReferenceColumn getFixture() {
		return (ReferenceColumn)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(MigrationFactory.eINSTANCE.createReferenceColumn());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link migration.ReferenceColumn#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see migration.ReferenceColumn#getName()
	 * @generated
	 */
	public void testGetName() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ReferenceColumnTest
